# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 06-24-2020

### Added
- Support for SQLite store ([#3](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/3))
- Support for S3 store ([#10](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/10))

### Changed
- Better binary link and checkum ([commit](https://gitlab.com/mrman/redis-bootleg-backup/-/commit/3194924c83f1085ba5f87bf926f31b15a6614612))
- Use `thiserror` for errors ([#5](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/5))
- Refactor shared testing code ([#12](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/12))

### Fixed
- Deduplicate keys in file listing for errors ([#8](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/8))

## [0.1.0] - 06-06-2020

### Added

- Add support for folder based backups ([#1](https://gitlab.com/mrman/redis-bootleg-backup/-/issues/1))
- Add `CHANGELOG.md`

[unreleased]: https://gitlab.com/mrman/redis-bootleg-backup/-/compare/v0.2.0...master
[0.2.0]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.2.0
[0.1.0]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.1.0
