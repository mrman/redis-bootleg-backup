.PHONY: get-version get-project-name all build build-release build-test \
				build-watch build-test-watch clean \
				run test test-unit test-unit-debug test-int test-int-debug test-e2e test-e2e-debug watch \
				check-tool-cargo check-tool-cargo-watch \
				builder-image builder-publish image publish registry-login get-version \
				local-db-setup local-db local-db-clean local-db-migrate local-db-psql local-db-remove

all: build

VERSION=$(shell perl -ne '/version\s+=\s+"([0-9|\.]+)"$$/ && print $$1 and last' Cargo.toml)

PROJECT_NAME ?= $(shell perl -ne '/name\s*=\s*"(.+)"$$/ && print $$1 and last' Cargo.toml)
IMAGE_NAME ?= cli
BUILDER_IMAGE_NAME ?= builder
REGISTRY_PATH = registry.gitlab.com/mrman/$(PROJECT_NAME)
FQ_IMAGE_NAME = $(REGISTRY_PATH)/$(IMAGE_NAME):$(VERSION)
FQ_BUILDER_IMAGE_NAME = $(REGISTRY_PATH)/$(BUILDER_IMAGE_NAME):$(VERSION)

BUILD_TARGET ?= x86_64-unknown-linux-musl
CARGO_BUILD_FLAGS ?= --target $(BUILD_TARGET)

DOCKER ?= $(shell command -v docker 2> /dev/null)
CARGO ?= $(shell command -v cargo 2> /dev/null)
CARGO_WATCH ?= $(shell command -v cargo-watch 2> /dev/null)
SHA256SUM ?= $(shell command -v sha256sum 2> /dev/null)
SHA1SUM ?= $(shell command -v sha1sum 2> /dev/null)
GIT ?= $(shell command -v git 2> /dev/null)

RELEASES_DIR ?= releases
RELEASE_BINARY_NAME ?= rbb
CARGO_TARGET_DIR ?= target
RELEASE_BUILT_BIN_PATH = $(CARGO_TARGET_DIR)/$(BUILD_TARGET)/release/$(PROJECT_NAME)

check-tool-cargo:
ifndef CARGO
	$(error "`cargo` is not available please install cargo (https://github.com/rust-lang/cargo/)")
endif

check-tool-cargo-watch:
ifndef CARGO_WATCH
	$(error "`cargo-watch` is not available please install cargo-watch (https://github.com/passcod/cargo-watch)")
endif

check-tool-sha256sum:
ifndef SHA256SUM
	$(error "`sha256sum` is not available please install sha256sum")
endif

check-tool-sha1sum:
ifndef SHA1SUM
	$(error "`sha1sum` is not available please install sha1sum")
endif

get-version:
	@echo -e -n ${VERSION}

get-project-name:
	@echo -e -n ${PROJECT_NAME}

dev-setup:
	@echo "[info] setting up development tooling..."
	cp .dev/git/hooks/pre-push .git/hooks && chmod +x .git/hooks/pre-push
	@echo "[info] installing rust utilities..."
	$(CARGO) install cargo-watch

fmt:
	$(CARGO) fmt

clean: check-tool-cargo local-db-clean
	$(CARGO) clean

build: check-tool-cargo
	$(CARGO) build $(CARGO_BUILD_FLAGS)

build-release: check-tool-cargo check-tool-sha1sum check-tool-sha256sum
	$(CARGO) build $(CARGO_BUILD_FLAGS) --release
	cp $(RELEASE_BUILT_BIN_PATH) $(RELEASES_DIR)/$(RELEASE_BINARY_NAME)

release: build-release check-tool-sha1sum check-tool-sha256sum
	$(SHA256SUM) releases/rbb > releases/rbb.sha256sum
	$(SHA1SUM) releases/rbb > releases/rbb.sha1sum
	$(GIT) add .
	$(GIT) commit -am "Release v$(VERSION)"
	$(GIT) tag v$(VERSION) HEAD
	$(GIT) push origin v$(VERSION)

build-test: check-tool-cargo
	$(CARGO) test $(CARGO_BUILD_FLAGS) --no-run

install: check-tool-cargo
	$(CARGO) install --path . redis-bootleg-backup-api

run:
	$(CARGO) run

test: test-unit test-int test-e2e

test-unit:
	$(CARGO) test $(CARGO_BUILD_FLAGS)

test-unit-debug:
	$(CARGO) test $(CARGO_BUILD_FLAGS) -- --nocapture

test-int:
	$(CARGO) test $(CARGO_BUILD_FLAGS) -- --ignored _int

test-int-debug:
	$(CARGO) test $(CARGO_BUILD_FLAGS) -- --ignored _int --nocapture

test-e2e: build-release
	$(CARGO) test $(CARGO_BUILD_FLAGS) -- --ignored _e2e

test-e2e-debug: build-release
	$(CARGO) test $(CARGO_BUILD_FLAGS) -- --ignored _e2e --nocapture

build-watch: check-tool-cargo check-tool-cargo-watch
	$(CARGO_WATCH) -x "build $(CARGO_BUILD_FLAGS)" --watch src

build-test-watch: check-tool-cargo check-tool-cargo-watch
	$(CARGO_WATCH) -x "test $(CARGO_BUILD_FLAGS)" --watch src --watch tests

#############
# Packaging #
#############

image:
		docker build -f infra/docker/Dockerfile -t $(FQ_IMAGE_NAME) .

builder-image:
		docker build -f infra/docker/builder.Dockerfile -t $(FQ_BUILDER_IMAGE_NAME) .

alpine-builder-image:
		docker build -f infra/docker/alpine-builder.Dockerfile -t $(FQ_BUILDER_IMAGE_NAME) .

registry-login:
		cat infra/secrets/ci-deploy-token-password.secret | \
		docker login -u $(shell cat infra/secrets/ci-deploy-token-username.secret) --password-stdin registry.gitlab.com

publish:
		docker push $(FQ_IMAGE_NAME)

builder-publish:
		docker push $(FQ_BUILDER_IMAGE_NAME)
