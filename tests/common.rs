use std::env;
use std::fs;
use std::thread;
use std::time;
use std::io::{BufReader, BufRead};
use std::path::PathBuf;
use std::process::{Command, Child, Stdio};
use std::time::SystemTime;
use std::collections::HashMap;
use std::net::TcpListener;

use redis::{Commands, Connection};
use rand::{thread_rng, Rng};

use redis_bootleg_backup::types::{BackupName, BackupStore, BackupExecutorType, BackupStoreType, RedisKey};

use uuid::Uuid;

use redis_bootleg_backup::stores::folder::v1::{FolderStore, FolderStoreOpts};

/// Redis defaults
const REDIS_IMAGE_NAME: &'static str = "redis";
const REDIS_DEFAULT_PORT: u32 = 6379;
const REDIS_DEFAULT_VERSION :&'static str = "5.0.3"; // shoudl match available binary in CI builder image

/// Helper for creating a  store with a temporary test folder
#[allow(dead_code)]
pub fn make_test_store() -> Result<FolderStore, String> {
    // Create a temp folder path
    let uuid = Uuid::new_v4().to_simple().to_string();
    let base_dir = env::temp_dir().join(uuid);

    // Create the folder and set it as the base dir
    fs::create_dir(&base_dir).map_err(|_| String::from("Failed to create temp dir for test"))?;

    Ok(FolderStore::with_opts(FolderStoreOpts { base_dir })?)
}

/// Make a temporary directory
pub fn make_temp_dir() -> Result<PathBuf, String> {
    // Create a temp folder path
    let uuid = Uuid::new_v4().to_simple().to_string();
    let dir = env::temp_dir().join("redis-bootleg-backup-test").join(uuid);

    // Create the folder and set it as the base dir
    fs::create_dir_all(&dir)
        .map_err(|_| String::from("Failed to create temp dir for test"))?;

    Ok(dir)
}

/// Represents a dockerized or launched-binary redis instance
pub struct InstanceInfo {
    pub process: Child,
    pub client_port: u32,

    /// Only populated if the instance is a dockerized instance
    pub container_name: Option<String>
}

const CONTAINER_DEFAULT_START_TIMEOUT_SECONDS: u64 = 20;

/// Helper for getting the docker binary path
pub fn get_docker_bin_path() -> String {
    env::var("DOCKER_BIN").unwrap_or(String::from("/usr/bin/docker"))
}

/// Enum to control which type of redis instance is made
#[derive(Debug, PartialEq)]
pub enum InstanceType {
    Dockerized,
    LaunchedBinary,
}

/// Helper function for starting a dockerized redis sub-process
pub fn start_redis_test_instance(maybe_instance_type: Option<InstanceType>) -> Result<InstanceInfo, String> {
    // Generate a random port for redis to run on
    let client_port: u32 = pick_random_tcp_port()?;

    // Determine which type of redis instance to make
    let instance_type = maybe_instance_type.unwrap_or(
        env::var("CI").map(|_| InstanceType::LaunchedBinary).unwrap_or(InstanceType::Dockerized)
    );

    let mut instance_info: InstanceInfo;
    // If we're in the Gitlab CI environment, we have to use a launched-binary redis-server
    if instance_type == InstanceType::LaunchedBinary {
        instance_info = InstanceInfo {
            client_port,
            container_name: None,
            process: Command::new("redis-server")
                .args(&[
                    "--port", format!("{}", client_port).as_str(),
                ])
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .spawn()
                .map_err(|e| format!("Failed to start child process: {:#?}", e))?
        };

    } else {
        let docker_bin_path = get_docker_bin_path();
        let redis_image = env::var("REDIS_IMAGE")
            .unwrap_or(format!("{}:{}", REDIS_IMAGE_NAME, REDIS_DEFAULT_VERSION));
        let container_name = format!("rbb-test-redis-{}", Uuid::new_v4().to_simple().to_string());

    // For local development we can use `docker` to avoid polluting host systems with redis-server
        instance_info = InstanceInfo {
            client_port,
            container_name: Some(container_name.clone()),
            process: Command::new(docker_bin_path)
            .args(&[
                "run",
                "-p", format!("{}:{}", client_port, REDIS_DEFAULT_PORT).as_str(),
                "--name", container_name.as_str(),
                redis_image.as_str(),
            ])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
                .map_err(|e| format!("Failed to start child process: {:#?}", e))?
        }
    }

    // Get the stdout
    if instance_info.process.stdout.is_none() {
        return Err(String::from("Process didn't have any stdout to read"));
    }
    let mut stdout = instance_info.process.stdout.unwrap();

    // Wait for startup message
    let now =  SystemTime::now();
    let mut buffered_output = BufReader::new(&mut stdout);
    loop {
        let mut line = String::new();
        // WARNING: this blocks, so if you pick the wrong
        buffered_output.read_line(&mut line)
            .map_err(|e| format!("failed to read line: {}", e))?;

        // Stop waiting if we see the line
        if line.contains("Ready to accept connections") {
            break
        }

        let elapsed = SystemTime::now().duration_since(now)
            .map_err(|e| format!("failed to get current time, during timeout checks: {:#?}", e))?;
        let timed_out = elapsed.as_secs() >= CONTAINER_DEFAULT_START_TIMEOUT_SECONDS;
        if timed_out {
            return Err(String::from("timed out waiting for start up line from redis container"));
        }
    }

    // Put the stdout object back in the process
    instance_info.process.stdout = Some(stdout);

    Ok(instance_info)
}

/// Helper function for picking a random port
pub fn pick_random_tcp_port() -> Result<u32, String> {
    pick_random_tcp_port_in_range(None, None)
}

const DEFAULT_MIN_PORT: u32 = 10000;
const DEFAULT_MAX_PORT: u32 = 20000;

/// Get a available random port with TCP
/// NOTE: unfortunately this function does *not* check if the port is in use
pub fn pick_random_tcp_port_in_range(min: Option<u32>, max: Option<u32>) -> Result<u32, String> {
    let port_min = min.unwrap_or(DEFAULT_MIN_PORT);
    let port_max = max.unwrap_or(DEFAULT_MAX_PORT);

    // Ensure we have enough to choose from for port min/max
    if port_min >= port_max || port_max - port_min < 100 {
        return Err(String::from("range too narrow for port picking"));
    }

    // Generate a random port for minio to run on
    let mut rng = thread_rng();
    let mut attempts: u32 = 0;
    while let port = rng.gen_range(port_min, port_max) {
        // Try a maximum of 100 times
        if attempts > 100 {
            break;
        }

        // If we are able to bind a tcp listener, immediately drop it and offer the port for use
        if let Ok(listener) = TcpListener::bind(format!("127.0.0.1:{}", port)) {
            // This listener should get dropped as soon as we attempt to leave
            return Ok(port);
        }

        attempts += 1;
    }

    Err(String::from("Failed to pick port"))
}

/// Minio defaults
const MINIO_IMAGE_NAME: &'static str = "minio/minio";
const MINIO_DEFAULT_PORT: u32 = 9000;
const MINIO_DEFAULT_VERSION :&'static str = "RELEASE.2020-06-18T02-23-35Z";
const MINIO_DEFAULT_DATA_PATH: &'static str = "/data";
pub const MINIO_DEFAULT_ACCESS_KEY_ID: &'static str = "minioadmin";
pub const MINIO_DEFAULT_SECRET_ACCESS_KEY: &'static str = "minioadmin";
pub const MINIO_BIN_PATH: &'static str = "/bin/minio";

/// Helper function for starting a dockerized minio sub-process
pub fn start_minio_test_instance(
    minio_data_dir: PathBuf,
    maybe_instance_type: Option<InstanceType>,
) -> Result<InstanceInfo, String> {
    let client_port = pick_random_tcp_port()?;

    // Determine which type of minio instance to make
    let instance_type = maybe_instance_type.unwrap_or(
        env::var("CI").map(|_| InstanceType::LaunchedBinary).unwrap_or(InstanceType::Dockerized)
    );

    let mut instance_info: InstanceInfo;
    // If we're in the Gitlab CI environment, we have to use a launched-binary minio-server
    if instance_type == InstanceType::LaunchedBinary {
        instance_info = InstanceInfo {
            client_port,
            container_name: None,
            process: Command::new(MINIO_BIN_PATH)
                .args(&[
                    "server",
                    "--address", format!("127.0.0.1:{}", client_port).as_str(),
                    &minio_data_dir.to_string_lossy(),
                ])
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .spawn()
                .map_err(|e| format!("Failed to start child process: {:#?}", e))?
        };

    } else {
        let docker_bin_path = get_docker_bin_path();
        let minio_image = env::var("MINIO_IMAGE")
            .unwrap_or(format!("{}:{}", MINIO_IMAGE_NAME, MINIO_DEFAULT_VERSION));
        let container_name = format!("rbb-test-minio-{}", Uuid::new_v4().to_simple().to_string());

    // For local development we can use `docker` to avoid polluting host systems with minio-server
        instance_info = InstanceInfo {
            client_port,
            container_name: Some(container_name.clone()),
            process: Command::new(docker_bin_path)
            .args(&[
                "run",
                "-p", format!("127.0.0.1:{}:{}", client_port, MINIO_DEFAULT_PORT).as_str(),
                "-v", format!("{}:/data", &minio_data_dir.to_string_lossy()).as_str(),
                "--name", container_name.as_str(),
                minio_image.as_str(),
                "server",
                "/data",
            ])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
                .map_err(|e| format!("Failed to start child process: {:#?}", e))?
        }
    }

    // Get the stdout
    if instance_info.process.stdout.is_none() {
        return Err(String::from("Process didn't have any stdout to read"));
    }
    let mut stdout = instance_info.process.stdout.unwrap();

    // Wait for startup message
    let now =  SystemTime::now();
    let mut buffered_output = BufReader::new(&mut stdout);
    loop {
        let mut line = String::new();
        // WARNING: this blocks, so if you pick the wrong
        buffered_output.read_line(&mut line)
            .map_err(|e| format!("failed to read line: {}", e))?;

        // Stop waiting if we see the line
        if line.contains("Object API (Amazon S3 compatible):") {
            // Since there is no exact "ready to serve connections" line,
            // add a slight wait to prevent attempting to connect too quickly
            thread::sleep(time::Duration::from_millis(500));
            break
        }

        let elapsed = SystemTime::now().duration_since(now)
            .map_err(|e| format!("failed to get current time, during timeout checks: {:#?}", e))?;
        let timed_out = elapsed.as_secs() >= CONTAINER_DEFAULT_START_TIMEOUT_SECONDS;
        if timed_out {
            return Err(String::from("timed out waiting for start up line from minio"));
        }
    }

    // Put the stdout object back in the process
    instance_info.process.stdout = Some(stdout);

    Ok(instance_info)
}

/// Helper function for stoping a redis test instance
pub fn stop_test_instance(mut instance_info: InstanceInfo) -> Result<(), String> {
    // If the instance is dockerized, it should have a container_name
    // we'll want to stop & remove it with docker
    if instance_info.container_name.is_some() {
        let docker_bin_path = get_docker_bin_path();
        let container_name = instance_info.container_name.clone().unwrap();

        let mut stop_cmd = Command::new(&docker_bin_path)
            .args(&[
                "stop",
                container_name.as_str()
            ])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .map_err(|e| format!("Failed to spawn stop container command: {:#?}", e))?;

        let _ = stop_cmd.wait()
            .map_err(|e| format!("Failed to wait for container stopping: {:#?}", e))?;

        let mut remove_cmd = Command::new(&docker_bin_path)
            .args(&[
                "rm",
                container_name.as_str()
            ])
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()
            .map_err(|e| format!("Failed to spawn remove container command: {:#?}", e))?;

        let _ = remove_cmd
            .wait()
            .map_err(|e| format!("Failed to wait for container removal: {:#?}", e))?;
    }

    // Kill the process
    instance_info.process
        .kill()
        .map_err(|e| format!("Failed to kill child process: {:#?}", e))?;

    Ok(())
}

/// Shared test suite that performs a basic backup for an integration test
/// this test adds a couple keys and makes sure the backup contains them
pub fn basic_backup_int_test(
    env_map: HashMap<String, String>,
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
) -> Result<(), String> {
    let dockerized_redis = start_redis_test_instance(None)?;

    // Connect to redis instance
    let uri = format!("redis://localhost:{}", &dockerized_redis.client_port);
    let client = redis::Client::open(uri.clone())
        .map_err(|e| format!("failed to create redis client: {:#?}", e))?;
    let mut conn = client.get_connection()
        .map_err(|e| format!("failed to connect to redis: {:#?}", e))?;

    // Write some keys
    let _: () = conn.set("test-key-1", "test-1").map_err(|e| format!("failed to write: {:#?}", e))?;
    let _: () = conn.set("test-key-2", "test-2").map_err(|e| format!("failed to write: {:#?}", e))?;

    // Perform a backup on the redis instance
    let backup = redis_bootleg_backup::backup(
        executor_type,
        store_type,
        uri.clone(),
        BackupName::from("test-backup"),
        &env_map,
    )?;

    // Ensure backup contains the expected/written keys
    assert_eq!(
        backup.get_value(RedisKey::from("test-key-1")),
        Ok(String::from("test-1").into_bytes()),
        "test-key-1 value matches",
    );

    // Ensure backup contains the expected/written keys
    assert_eq!(
        backup.get_value(RedisKey::from("test-key-2")),
        Ok(String::from("test-2").into_bytes()),
        "test-key-2 value matches",
    );

    // Clean up the dockerized redis container
    stop_test_instance(dockerized_redis)?;

    Ok(())
}


/// Shared test suite that performs a basic restore for an integration test
/// this test adds a couple keys and makes sure the backup contains them
pub fn basic_restore_int_test(
    env_map: HashMap<String, String>,
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
) -> Result<(), String> {
    let dockerized_redis = start_redis_test_instance(None)?;

    // Connect to redis instance
    let uri = format!("redis://localhost:{}", &dockerized_redis.client_port);
    let client = redis::Client::open(uri.clone())
        .map_err(|e| format!("failed to create redis client: {:#?}", e))?;
    let mut conn = client.get_connection()
        .map_err(|e| format!("failed to connect to redis: {:#?}", e))?;

    // Ensure that test-key-1 and test-key-2 are not already set
    assert_eq!(
        conn.get::<&str, Option<String>>("test-key-1").map_err(|e| format!("failed to get: {:#?}", e))?,
        None,
        "test-key-1 was not present",
    );
    assert_eq!(
        conn.get::<&str, Option<String>>("test-key-2").map_err(|e| format!("failed to get: {:#?}", e))?,
        None,
        "test-key-2 was not present",
    );

    // Perform a restore from an existing fixture (specific to version)
    redis_bootleg_backup::restore(
        executor_type,
        store_type,
        uri.clone(),
        BackupName::from("v1-simple"),
        &env_map,
    ).expect("failed to perform restore");

    // Ensure that test-key-1 and test-key-2 have been restored by the restore
    assert_eq!(
        conn.get::<&str, Option<String>>("test-key-1").expect("failed get test-key-1"),
        Some(String::from("test-1")),
        "test-key-1 was present after restore",
    );
    assert_eq!(
        conn.get::<&str, Option<String>>("test-key-2").expect("failed get test-key-2"),
        Some(String::from("test-2")),
        "test-key-2 was present after restore",
    );

    // Clean up the dockerized redis container
    stop_test_instance(dockerized_redis)?;

    Ok(())
}

/// Path to the built release binary
/// NOTE: the binary *must* be built for release (via `make build-release`) before E2E tests are run
pub const RELEASE_BIN_PATH: &'static str = "releases/rbb";

/// Shared test suite that performs a basic backup for an E2E test
/// this test adds a couple keys and makes sure the backup contains them
/// but importantly runs the built binary itself, "from the outside"
pub fn basic_backup_e2e_test<PreCheckFn, PostCheckFn, StoreBuilder>(
    env_map: HashMap<String, String>,
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
    pre_check: PreCheckFn,
    post_check: PostCheckFn,
    store_builder: StoreBuilder,
) -> Result<(), String>
where
    PreCheckFn: FnOnce() -> Result<(), String>,
    PostCheckFn: FnOnce() -> Result<(), String>,
    StoreBuilder: FnOnce() -> Result<Box<dyn BackupStore>, String>
{
    let dockerized_redis = start_redis_test_instance(None)?;

    // Connect to redis instance
    let uri = format!("redis://localhost:{}", &dockerized_redis.client_port);
    let client = redis::Client::open(uri.clone())
        .map_err(|e| format!("failed to create redis client: {:#?}", e))?;
    let mut conn = client.get_connection()
        .map_err(|e| format!("failed to connect to redis: {:#?}", e))?;

    // Write some keys
    let _: () = conn.set("test-key-1", "test-1").map_err(|e| format!("failed to write: {:#?}", e))?;
    let _: () = conn.set("test-key-2", "test-2").map_err(|e| format!("failed to write: {:#?}", e))?;

    // // Create a temporary directory which will store the backup
    let backup_name = BackupName::from("test-backup");

    // Run pre-backup check
    pre_check()?;

    // Perform a backup via CLI
    let mut cmd = Command::new(RELEASE_BIN_PATH);
    cmd.args(&[
            "--executor-type", executor_type.to_string().as_str(),
            "--store-type", store_type.to_string().as_str(),
            "backup",
            "--name", backup_name.as_str(),
            "--uri", &uri.clone(),
        ]);

    // Add all the provided env variables
    for (k,v) in env_map {
        cmd.env(k, v);
    }

    let output = cmd
        .output()
        .map_err(|e| format!("Failed to spawn backup command: {:#?}", e))?;

    assert_eq!(output.stderr.len(), 0, "stderr is empty (no errors occurred)");

    // Run post-backup check
    post_check()?;

    // build the store
    let store = store_builder()?;
    let backup = store.get_backup(String::from(backup_name))?;

    // Ensure backup contains the expected/written keys
    assert_eq!(
        String::from_utf8(backup.get_value(RedisKey::from("test-key-1"))?)
            .map_err(|e| format!("failed to convert test-key-1 value: {}", e))?,
        String::from("test-1"),
        "test-key-1 value matches",
    );

    // Ensure backup contains the expected/written keys
    assert_eq!(
        String::from_utf8(backup.get_value(RedisKey::from("test-key-2"))?)
            .map_err(|e| format!("failed to convert test-key-2 value: {}", e))?,
        String::from("test-2"),
        "test-key-2 value matches",
    );

    // Clean up the dockerized redis container
    stop_test_instance(dockerized_redis)?;

    Ok(())
}

/// Shared test suite that performs a basic restore for an E2E test
/// this test adds a couple keys and makes sure the restore contains them
/// but importantly runs the built binary itself, "from the outside"
pub fn basic_restore_e2e_test<PreCheckFn, PostCheckFn>(
    env_map: HashMap<String, String>,
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
    backup_name: BackupName,
    pre_check: PreCheckFn,
    post_check: PostCheckFn,
) -> Result<(), String>
where
    PreCheckFn: FnOnce(&mut Connection) -> Result<(), String>,
    PostCheckFn: FnOnce(&mut Connection) -> Result<(), String>,
{
    let dockerized_redis = start_redis_test_instance(None)?;

    // Connect to redis instance
    let uri = format!("redis://localhost:{}", &dockerized_redis.client_port);
    let client = redis::Client::open(uri.clone())
        .map_err(|e| format!("failed to create redis client: {:#?}", e))?;
    let mut conn = client.get_connection()
        .map_err(|e| format!("failed to connect to redis: {:#?}", e))?;

    pre_check(&mut conn)?;

    // Perform a restore via CLI
    let mut cmd = Command::new(RELEASE_BIN_PATH);
    cmd.args(&[
            "--executor-type", executor_type.to_string().as_str(),
            "--store-type", store_type.to_string().as_str(),
            "restore",
            "--name", backup_name.as_str(),
            "--uri", &uri.clone(),
        ]);

    // Add all the provided env variables
    for (k,v) in env_map {
        cmd.env(k, v);
    }

    let output = cmd
        .output()
        .map_err(|e| format!("Failed to spawn restore command: {:#?}", e))?;

    assert_eq!(output.stderr.len(), 0, "stderr is empty (no errors occurred)");

    post_check(&mut conn)?;

    // Clean up the dockerized redis container
    stop_test_instance(dockerized_redis)?;

    Ok(())
}
