use std::process::Command;
use std::fs;
use std::path::PathBuf;

use fs_extra::copy_items;
use fs_extra::dir::CopyOptions;

use redis::{Commands, Connection};

use redis_bootleg_backup::types::{BackupName, RedisKey, BackupExecutorType, BackupStore, BackupStoreType};
use redis_bootleg_backup::stores::s3::v1::{S3Store, S3StoreOpts};
use redis_bootleg_backup::stores::build_backup_store;

mod common;

use crate::common::{
    MINIO_DEFAULT_ACCESS_KEY_ID,
    MINIO_DEFAULT_SECRET_ACCESS_KEY,
    make_temp_dir,
    basic_backup_e2e_test,
    basic_restore_e2e_test,
    start_minio_test_instance,
    stop_test_instance,
};

pub const FIXTURES_S3_V1_BACKUP_ROOT_DIR: &'static str = "tests/fixtures/s3-v1";

/// Path to the built release binary
pub const RELEASE_BIN_PATH: &'static str = "target/x86_64-unknown-linux-musl/debug/redis-bootleg-backup";

/// Test that CLI execution of a backup works with:
/// - executor: direct-v1
/// - store: s3-v1
#[test]
#[ignore]
fn cli_backup_direct_v1_to_s3_v1_e2e() -> Result<(), String> {
    let bucket_name = String::from("test-bucket");

    // Create and possibly pre-populate a folder with minio data
    let test_dir = make_temp_dir()?;
    let minio_data_dir = test_dir.join("minio-data");
    fs::create_dir(&minio_data_dir)
        .map_err(|e| format!("Failed to create minio data folder: {:?}", e))?;

    // Create the bucket so that it exists
    let bucket_dir = minio_data_dir.join(bucket_name.as_str());
    fs::create_dir(&bucket_dir)
        .map_err(|e| format!("Failed to create bucket dir inside minio data folder: {:?}", e))?;

    // Start a minio instance
    let minio = start_minio_test_instance(minio_data_dir, None)?;

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_S3_ENDPOINT"), format!("http://127.0.0.1:{}", &minio.client_port));
    env_map.insert(String::from("RBB_S3_BUCKET"), String::from("test-bucket"));
    env_map.insert(String::from("RBB_S3_ACCESS_KEY_ID"), String::from(MINIO_DEFAULT_ACCESS_KEY_ID));
    env_map.insert(String::from("RBB_S3_SECRET_ACCESS_KEY"), String::from(MINIO_DEFAULT_SECRET_ACCESS_KEY));
    let env_map_cloned = env_map.clone();

    // pre backup check should ensure the temp directory is empty *before* a backup is taken
    let pre_backup_check = || {
        // Before backup, the bucket directory should be empty
        let file_count = fs::read_dir(&bucket_dir)
            .map_err(|e| format!("Failed to count dir: {}", e))?
            .count();
        assert_eq!(file_count, 0, "temp dir is empty before backup");

        Ok(())
    };

    // post backup check should ensure the temp directory is *not* empty after a backup is taken
    let post_backup_check = || {
        // Ensure backup folder is not empty
        let file_count = fs::read_dir(&bucket_dir)
            .map_err(|e| format!("Failed to count dir: {}", e))?
            .count();
        assert_ne!(file_count, 0, "temp dir is populated after backup");

        Ok(())
    };

    // We need a way to build the store after the backup is taken
    let store_builder = || {
        // Create the store from the folder & ensure contained values are correct
        build_backup_store(BackupStoreType::S3V1, &env_map_cloned)
    };

    // Run the basic backup e2e test that uses the binary
    basic_backup_e2e_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::S3V1,
        pre_backup_check,
        post_backup_check,
        store_builder,
    )?;

    stop_test_instance(minio)
}

/// Test that CLI execution of a restore works with:
/// - executor: direct-v1
/// - store: s3-v1
#[test]
#[ignore]
fn cli_restore_v1_simple_direct_v1_to_s3_v1_e2e() -> Result<(), String> {
    let bucket_name = String::from("test-bucket");

    // Create and possibly pre-populate a folder with minio data
    let test_dir = make_temp_dir()?;
    let minio_data_dir = test_dir.join("minio-data");
    fs::create_dir(&minio_data_dir)
        .map_err(|e| format!("Failed to create minio data folder: {:?}", e))?;

    // Create the bucket so that it exists
    let bucket_dir = minio_data_dir.join(bucket_name.as_str());
    fs::create_dir(&bucket_dir)
        .map_err(|e| format!("Failed to create bucket dir inside minio data folder: {:?}", e))?;

    // Seed the minio instance with fixture data
    let options = CopyOptions::new();
    let fixture_dir = PathBuf::from(FIXTURES_S3_V1_BACKUP_ROOT_DIR)
        .join("v1-simple")
        .canonicalize()
        .expect("failed to get fixture dir");
    copy_items(
        &vec!(fixture_dir),
        bucket_dir,
        &options,
    )
        .map_err(|e| format!("Failed to seed minio: {:?}", e))?;

    // Start the minio instance
    let minio = start_minio_test_instance(minio_data_dir, None)?;

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_S3_ENDPOINT"), format!("http://127.0.0.1:{}", &minio.client_port));
    env_map.insert(String::from("RBB_S3_BUCKET"), String::from("test-bucket"));
    env_map.insert(String::from("RBB_S3_ACCESS_KEY_ID"), String::from(MINIO_DEFAULT_ACCESS_KEY_ID));
    env_map.insert(String::from("RBB_S3_SECRET_ACCESS_KEY"), String::from(MINIO_DEFAULT_SECRET_ACCESS_KEY));
    let env_map_cloned = env_map.clone();

    // pre check should ensure that the relevant keys for the fixture backup are *not* present already
    let pre_check = |conn: &mut Connection| {
        // Ensure that test-key-1 and test-key-2 are not already set
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-1").map_err(|e| format!("failed to get: {:#?}", e))?,
            None,
            "test-key-1 was not present",
        );
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-2").map_err(|e| format!("failed to get: {:#?}", e))?,
            None,
            "test-key-2 was not present",
        );

        Ok(())
    };

    // post check should ensure that the relevant keys for the fixture backup are *not* postsent already
    let post_check = |conn: &mut Connection| {
        // Ensure that test-key-1 and test-key-2 have been restored by the restore
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-1").expect("failed get test-key-1"),
            Some(String::from("test-1")),
            "test-key-1 was present after restore",
        );
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-2").expect("failed get test-key-2"),
            Some(String::from("test-2")),
            "test-key-2 was present after restore",
        );

        Ok(())
    };

    // Run the basic backup e2e test that uses the binary
    basic_restore_e2e_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::S3V1,
        BackupName::from("v1-simple"),
        pre_check,
        post_check,
    )?;

    stop_test_instance(minio)
}
