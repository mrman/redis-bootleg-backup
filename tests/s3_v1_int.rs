use std::path::PathBuf;
use std::fs;

use redis::Commands;
use fs_extra::copy_items;
use fs_extra::dir::CopyOptions;

use redis_bootleg_backup::types::{BackupExecutorType, BackupStoreType};
use redis_bootleg_backup::stores::s3::v1::KV_PREFIX_NAME;

mod common;

use crate::common::{
    MINIO_DEFAULT_ACCESS_KEY_ID,
    MINIO_DEFAULT_SECRET_ACCESS_KEY,
    basic_backup_int_test,
    basic_restore_int_test,
    start_minio_test_instance,
    stop_test_instance,
    make_temp_dir,
};

pub const FIXTURES_S3_V1_BACKUP_ROOT_DIR: &'static str = "tests/fixtures/s3-v1";

/// Test of writing and backing up simple redis keys & values
#[test]
#[ignore]
fn s3_store_backup_v1_simple_int() -> Result<(), String> {
    let bucket_name = String::from("test-bucket");

    // Create and possibly pre-populate a folder with minio data
    let test_dir = make_temp_dir()?;
    let minio_data_dir = test_dir.join("minio-data");
    fs::create_dir(&minio_data_dir)
        .map_err(|e| format!("Failed to create minio data folder: {:?}", e))?;

    // Create the bucket so that it exists
    let bucket_dir = minio_data_dir.join(bucket_name.as_str());
    fs::create_dir(&bucket_dir)
        .map_err(|e| format!("Failed to create bucket dir inside minio data folder: {:?}", e))?;

    // Start a minio instance
    let minio = start_minio_test_instance(minio_data_dir, None)?;

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_S3_ENDPOINT"), format!("http://127.0.0.1:{}", &minio.client_port));
    env_map.insert(String::from("RBB_S3_BUCKET"), String::from("test-bucket"));
    env_map.insert(String::from("RBB_S3_ACCESS_KEY_ID"), String::from(MINIO_DEFAULT_ACCESS_KEY_ID));
    env_map.insert(String::from("RBB_S3_SECRET_ACCESS_KEY"), String::from(MINIO_DEFAULT_SECRET_ACCESS_KEY));

    common::basic_backup_int_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::S3V1,
    )?;

    stop_test_instance(minio)
}

/// Test of restoring and reading simple redis keys and values
/// note that this test relies on the fixtures/archives/<version>/backups/simple s3 being present
#[test]
#[ignore]
fn s3_store_restore_v1_simple_int() -> Result<(), String> {
    let bucket_name = String::from("test-bucket");

    // Create and possibly pre-populate a folder with minio data
    let test_dir = make_temp_dir()?;
    let minio_data_dir = test_dir.join("minio-data");
    fs::create_dir(&minio_data_dir)
        .map_err(|e| format!("Failed to create minio data folder: {:?}", e))?;

    // Create the contents of the data directory as the store would expect
    // NOTE: this code will likely break as implementation changes
    let bucket_dir = minio_data_dir.join(bucket_name.as_str());
    fs::create_dir(&bucket_dir)
        .map_err(|e| format!("Failed to create bucket dir inside minio data folder: {:?}", e))?;

    // Seed the minio instance with fixture data
    let options = CopyOptions::new();
    let fixture_dir = PathBuf::from(FIXTURES_S3_V1_BACKUP_ROOT_DIR)
        .join("v1-simple")
        .canonicalize()
        .expect("failed to get fixture dir");
    copy_items(
        &vec!(fixture_dir),
        bucket_dir,
        &options,
    )
        .map_err(|e| format!("Failed to seed minio: {:?}", e))?;

    // Start a minio instance
    let minio = start_minio_test_instance(minio_data_dir, None)?;

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_S3_ENDPOINT"), format!("http://127.0.0.1:{}", &minio.client_port));
    env_map.insert(String::from("RBB_S3_BUCKET"), bucket_name.clone());
    env_map.insert(String::from("RBB_S3_ACCESS_KEY_ID"), String::from(MINIO_DEFAULT_ACCESS_KEY_ID));
    env_map.insert(String::from("RBB_S3_SECRET_ACCESS_KEY"), String::from(MINIO_DEFAULT_SECRET_ACCESS_KEY));

    // Since we can't really store a fixture for the s3

    common::basic_restore_int_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::S3V1,
    )?;

    stop_test_instance(minio)
}
