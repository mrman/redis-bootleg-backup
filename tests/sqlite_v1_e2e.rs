use std::process::Command;
use std::fs;
use std::path::PathBuf;

use redis::{Commands, Connection};

// use redis_bootleg_backup::stores::sqlite::v1::{SQLiteStore, SQLiteStoreOpts};
use redis_bootleg_backup::types::{BackupName, RedisKey, BackupExecutorType, BackupStore, BackupStoreType};
use redis_bootleg_backup::stores::sqlite::v1::{SQLiteStore, SQLiteStoreOpts};
use redis_bootleg_backup::stores::build_backup_store;

mod common;

use crate::common::{
    make_temp_dir,
    basic_backup_e2e_test,
    basic_restore_e2e_test,
};

/// Path to the built release binary
pub const RELEASE_BIN_PATH: &'static str = "target/x86_64-unknown-linux-musl/debug/redis-bootleg-backup";

pub const FIXTURES_SQLITE_V1_BACKUP_ROOT_DIR: &'static str = "tests/fixtures/sqlite-v1/backup-root";

/// Test that CLI execution of a backup works with:
/// - executor: direct-v1
/// - store: sqlite-v1
#[test]
#[ignore]
fn cli_backup_direct_v1_to_sqlite_v1_e2e() -> Result<(), String> {
    // Create temporary directory to store output
    let backup_output_dir = make_temp_dir()?;

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_SQLITE_BASE_DIR"), String::from(backup_output_dir.to_string_lossy()));
    let env_map_cloned = env_map.clone();

    // pre backup check should ensure the temp directory is empty *before* a backup is taken
    let pre_backup_check = || {
        // Before backup, the temp dir should be empty
        let file_count = fs::read_dir(&backup_output_dir)
            .map_err(|e| format!("Failed to count dir: {}", e))?
            .count();
        assert_eq!(file_count, 0, "temp dir is empty before backup");

        Ok(())
    };

    // post backup check should ensure the temp directory is *not* empty after a backup is taken
    let post_backup_check = || {
        // Ensure backup folder is not empty
        let file_count = fs::read_dir(&backup_output_dir)
            .map_err(|e| format!("Failed to count dir: {}", e))?
            .count();
        assert_ne!(file_count, 0, "temp dir is populated after backup");

        Ok(())
    };

    // We need a way to build the store after the backup is taken
    let store_builder = || {
        // Create the store from the folder & ensure contained values are correct
        build_backup_store(BackupStoreType::SQLiteV1, &env_map_cloned)
    };

    // Run the basic backup e2e test that uses the binary
    basic_backup_e2e_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::SQLiteV1,
        pre_backup_check,
        post_backup_check,
        store_builder,
    )
}

/// Test that CLI execution of a restore works with:
/// - executor: direct-v1
/// - store: sqlite-v1
#[test]
#[ignore]
fn cli_restore_v1_simple_direct_v1_to_sqlite_v1_e2e() -> Result<(), String> {
    // Use the backup fixtures to perform a restore
    let backup_root_dir = PathBuf::from(FIXTURES_SQLITE_V1_BACKUP_ROOT_DIR).canonicalize().expect("fixtures abs path failed");

    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    env_map.insert(String::from("RBB_SQLITE_BASE_DIR"), backup_root_dir.to_string_lossy().into_owned());

    // pre check should ensure that the relevant keys for the fixture backup are *not* present already
    let pre_check = |conn: &mut Connection| {
        // Ensure that test-key-1 and test-key-2 are not already set
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-1").map_err(|e| format!("failed to get: {:#?}", e))?,
            None,
            "test-key-1 was not present",
        );
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-2").map_err(|e| format!("failed to get: {:#?}", e))?,
            None,
            "test-key-2 was not present",
        );

        Ok(())
    };

    // post check should ensure that the relevant keys for the fixture backup are *not* postsent already
    let post_check = |conn: &mut Connection| {
        // Ensure that test-key-1 and test-key-2 have been restored by the restore
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-1").expect("failed get test-key-1"),
            Some(String::from("test-1")),
            "test-key-1 was present after restore",
        );
        assert_eq!(
            conn.get::<&str, Option<String>>("test-key-2").expect("failed get test-key-2"),
            Some(String::from("test-2")),
            "test-key-2 was present after restore",
        );

        Ok(())
    };

    // Run the basic backup e2e test that uses the binary
    basic_restore_e2e_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::SQLiteV1,
        BackupName::from("v1-simple"),
        pre_check,
        post_check,
    )
}
