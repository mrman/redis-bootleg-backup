use std::path::PathBuf;
use redis::Commands;

use redis_bootleg_backup::types::{BackupExecutorType, BackupStoreType};

mod common;

use crate::common::{basic_backup_int_test, basic_restore_int_test, make_temp_dir};

pub const FIXTURES_FOLDER_V1_BACKUP_ROOT_DIR: &'static str = "tests/fixtures/folder-v1/backup-root";

/// Test of writing and backing up simple redis keys & values
#[test]
#[ignore]
fn folder_store_backup_v1_simple_int() -> Result<(), String> {
    // Build env map which will be used for storage options
    let mut env_map = std::collections::HashMap::new();
    let temp_dir = make_temp_dir()?;
    env_map.insert(String::from("RBB_FOLDER_BASE_DIR"), String::from(temp_dir.to_string_lossy()));

    common::basic_backup_int_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::FolderV1,
    )
}

/// Test of restoring and reading simple redis keys and values
/// note that this test relies on the fixtures/archives/<version>/backups/simple folder being present
#[test]
#[ignore]
fn folder_store_restore_v1_simple_int() -> Result<(), String> {
    let mut env_map = std::collections::HashMap::new();
    let fixtures_dir = PathBuf::from(FIXTURES_FOLDER_V1_BACKUP_ROOT_DIR).canonicalize().expect("fixtures abs path failed");
    env_map.insert(String::from("RBB_FOLDER_BASE_DIR"), String::from(fixtures_dir.to_string_lossy()));

    common::basic_restore_int_test(
        env_map,
        BackupExecutorType::DirectV1,
        BackupStoreType::FolderV1,
    )
}
