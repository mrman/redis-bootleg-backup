use log::{debug, info, error, warn};
use semver::Version;
use std::collections::HashMap;

use s3::creds::Credentials;
use s3::bucket::Bucket;
use s3::region::Region;
use s3::S3Error;

use crate::types::*;

/// Current version of the S3Store
const VERSION: &'static str = "1.0.0";

const VERSION_OBJ_NAME: &'static str = ".version";
pub const KV_PREFIX_NAME: &'static str = "kv";

/// Config for creating a restore from a given redis instance for a given folder on disk
/// like other custom executors/stores, this configuration must be read from environment
#[derive(Debug)]
pub struct S3StoreOpts {
    /// Storage instance that contains the backups
    pub storage: Storage,
}

const DEFAULT_REGION: &'static str = "us-east-1";
const DEFAULT_NAME: &'static str = "redis-backups";
const DEFAULT_LOCATION_SUPPORTED: bool = true;

/// Helper to check for successful response codes
pub fn is_success_code(code: u16) -> bool {
    code >= 200 && code <= 299
}

/// Contains all
#[derive(Debug, Clone)]
pub struct Storage {
    name: String,
    region: Region,
    credentials: Credentials,
    bucket: String,
    location_supported: bool,
}

impl S3StoreOpts {
    /// Build opts from env
    fn from_env_map(env_map: &HashMap<String, String>) -> Result<S3StoreOpts, BackupStoreError> {
        let endpoint = env_map.get("RBB_S3_ENDPOINT")
            .map(String::from)
            .ok_or(BackupStoreError::InvalidConfiguration(format!("failed to parse endpoint from ENV var RBB_S3_ENDPOINT")))?;

        let region_raw = env_map.get("RBB_S3_REGION")
            .map(String::from)
            .unwrap_or(String::from(DEFAULT_REGION));
        let region: Region = Region::Custom {
            region: region_raw.parse()
                .map_err(|e| BackupStoreError::InvalidConfiguration(format!("failed to parse region from ENV: {}", e)))?,
            endpoint: endpoint.into()
        };

        let bucket = env_map.get("RBB_S3_BUCKET")
            .map(String::from)
            .ok_or(BackupStoreError::InvalidConfiguration(format!("failed to parse bucket name from ENV var RBB_S3_BUCKET")))?;

        let location_supported = env_map.get("RBB_S3_LOCATION_SUPPORTED")
            .map(|v| v.to_lowercase() == "true")
            .unwrap_or(DEFAULT_LOCATION_SUPPORTED);

        let name = String::from(DEFAULT_NAME);

        // Build credentials from env
        let access_key = env_map.get("RBB_S3_ACCESS_KEY_ID").map(String::as_str);
        let secret_key = env_map.get("RBB_S3_SECRET_ACCESS_KEY").map(String::as_str);
        let security_token = env_map.get("RBB_S3_SECURITY_TOKEN").map(String::as_str);
        let session_token = env_map.get("RBB_S3_SESSION_TOKEN").map(String::as_str);
        let profile = env_map.get("RBB_S3_PROFILE").map(String::as_str);

        let credentials = Credentials::new_blocking(access_key, secret_key, security_token, session_token, profile.map(|p| p.into()))
            .map_err(|e| BackupStoreError::InvalidConfiguration(format!("failed to load credentials from profile: {}", e)))?;

        let storage = Storage {
            name,
            region,
            credentials,
            bucket,
            location_supported,
        };

        Ok(S3StoreOpts{ storage })
    }
}

/// S3 backend
/// The S3 Backend is a storage backend that writes and reads [S3](https://aws.amazon.com/s3/) for backups
/// keys are stored as individually named objects and the object data are the contained values
pub struct S3Store {
    /// Version of the backend
    version: Version,

    /// Options for the folder backend
    opts: S3StoreOpts,
}

impl S3Store {
    /// Create a new S3Store, with options pulled from the provided environment mapping
    pub fn new(env_map: &HashMap<String, String>) -> Result<S3Store, BackupStoreError> {
        return S3Store::with_opts(S3StoreOpts::from_env_map(env_map)?)
    }

    /// Create a S3Store with explicit options
    pub fn with_opts(opts: S3StoreOpts) -> Result<S3Store, BackupStoreError> {
        let version = Version::parse(VERSION)
                .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to parse version: {}", e)))?;

        Ok(S3Store {version, opts})
    }
}

impl BackupStore for S3Store {
    /// Get the backup backend enum type
    fn get_type(&self) -> BackupStoreType {
        return BackupStoreType::S3V1;
    }

    /// Get the version of the store itself, from S3
    fn get_store_version(&self) -> Result<Version, BackupStoreError> {
        debug!("[stores/s3-v1] reading version...");

        // Connect to s3 bucket
        let bucket = Bucket::new(&self.opts.storage.bucket, self.opts.storage.region.clone(), self.opts.storage.credentials.clone())?;

        // Read the file if present
        let (version_bytes, _) = bucket.get_object_blocking(format!("/{}", VERSION_OBJ_NAME))?;
        let version_raw = String::from_utf8(version_bytes)
            .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to parse version file UTF8 contents: {}", e)))?;
        let version = Version::parse(&version_raw)
            .map_err(|e| BackupStoreError::UnexpectedError(format!("Failed to parse version returned from S3:, {}", e)))?;

        Ok(version)
    }

    /// Initialize the backend, ensuring folder is created and lockfile is made
    fn init(&mut self) -> Result<(), BackupStoreError> {
        debug!("[stores/s3-v1] initializing");

        // It is possible that the store *does not exist* remotely yet
        // Since we're intiializing, we're going to write the version file
        let bucket = Bucket::new(&self.opts.storage.bucket, self.opts.storage.region.clone(), self.opts.storage.credentials.clone())?;
        let (data, code) = bucket.put_object_blocking(format!("/{}", VERSION_OBJ_NAME), self.version.to_string().as_bytes(), "text/plain")?;
        if !is_success_code(code) {
            if let Ok(resp) = String::from_utf8(data) {
                let msg = format!("[stores/s3-v1] Error while writing to S3: {}", resp);
                eprintln!("{}", msg);
                warn!("{}", msg);
            }
            return Err(BackupStoreError::FailedToCreateBackup(String::from("Failed to write version to s3")));
        }

        // Ensure ensure version is present & correct
        match self.get_store_version() {
            Ok(version) => {
                info!("[stores/s3-v1] detected remote (in-bucket) version [{:?}]", version);
                if VERSION != version.to_string().as_str() {
                    return Err(BackupStoreError::InvalidConfiguration(format!("version in bucket [{}] does not match", version.to_string())));
                }
            },
            Err(e) => {
                let msg = format!("[stores/s3-v1] unexpected error during initialization {}", e);
                eprintln!("{}", msg);
                error!("{}", msg);
                return Err(BackupStoreError::UnexpectedError(String::from(msg)));
            }
        };

        Ok(())
    }

    /// Cleanup the s3 backend
    fn cleanup(&mut self) -> Result<(), BackupStoreError> {
        Ok(())
    }

    /// Get or create a backup by creating appropriate DB on disk
    fn get_or_create_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        let backup = S3StoreBackup::create(self.opts.storage.clone(), self.version.clone(), backup_name)
            .map_err(|e| BackupStoreError::FailedToCreateBackup(format!("failed to create backup: {}", e)))?;
        Ok(Box::from(backup))
    }

    /// Get a backup
    fn get_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        // Retrieve the backup
        let backup = S3StoreBackup::get(self.opts.storage.clone(), backup_name)
            .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to retrieve backup: {}", e)))?;

        Ok(Box::from(backup))
    }

}

/// S3 backup
struct S3StoreBackup {
    /// Version of the backup (should match the store at time of creation)
    version: Version,

    /// Name of the backup
    name: BackupName,

    /// Storage instance that contains the backups
    storage: Storage,
}

impl From<S3Error> for BackupError {
    fn from(err: S3Error) -> BackupError {
        BackupError::UnexpectedError(format!("S3 error: {}", err))
    }
}

impl From<S3Error> for BackupStoreError {
    fn from(err: S3Error) -> BackupStoreError {
        BackupStoreError::UnexpectedError(format!("S3 error: {}", err))
    }
}

impl S3StoreBackup {
    fn get(storage: Storage, name: BackupName) -> Result<S3StoreBackup, BackupError> {
        // Read the version from the DB
        let version = S3StoreBackup::read_version_from_storage(&storage)?;

        let mut backup = S3StoreBackup{version, name, storage};

        // Initialize the backup
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }

    fn create(storage: Storage, version: Version, name: BackupName) -> Result<S3StoreBackup, BackupError> {
        // Create the backup
        let mut backup = S3StoreBackup{version, name, storage};

        // Initialize the backup
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }

    /// Initialize a given backup
    fn init(&mut self) -> Result<&mut Self, BackupError> {
        // Check the version to ensure it's supported (the version might not be written to S3 yet)
        self.check_version()?;

        // Write the version to S3
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;
        let (_, code) = bucket.put_object_blocking(format!("/{}", VERSION_OBJ_NAME), self.version.to_string().as_bytes(), "text/plain")?;
        if !is_success_code(code) {
            return Err(BackupError::UnexpectedError(String::from("Failed to write version")));
        }

        Ok(self)
    }

    /// Read the version of this backup from S3
    fn read_version_from_storage(storage: &Storage) -> Result<Version, BackupError> {
        // Open the bucket
        let bucket = Bucket::new(&storage.bucket, storage.region.clone(), storage.credentials.clone())?;

        // Retrieve the version object contents
        let (version_bytes, _) = bucket.get_object_blocking(format!("/{}", VERSION_OBJ_NAME))?;
        let version_raw = String::from_utf8(version_bytes)
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to parse backup version file UTF8 contents: {}", e)))?;

        // Parse the version contents
        Version::parse(&version_raw)
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to parse backup version returned from S3:, {}", e)))
    }

    /// Ensure that the version of the DB matches the in-memory version
    fn check_version(&self) -> Result<(), BackupError> {
        // Ensure version is one we support
        let supported_version = Version::parse(VERSION)
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to parse S3 version value:, {}", e)))?;

        if self.version != supported_version {
            return Err(BackupError::UnsupportedVersion(format!(
                "Version mismatch, current S3Store version [{}] does not support backups of version [{}]",
                supported_version,
                self.version,
            )))
        }

        Ok(())
    }
}

impl Backup for S3StoreBackup {
    fn get_store_type(&self) -> BackupStoreType {
        return BackupStoreType::S3V1;
    }

    fn get_version(&self) -> Result<Version, BackupError> {
        S3StoreBackup::read_version_from_storage(&self.storage)
    }

    fn get_name(&self) -> BackupName {
        self.name.clone()
    }

    fn get_key_count(&self) -> Result<u32, BackupError> {
        // Access the bucket
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;

        // Retrieve the items under the KV prefix
        let prefix = format!("/{}/{}", self.name, KV_PREFIX_NAME);
        let list = bucket.list_blocking(prefix, None)?;

        let mut count: u32 = 0;
        for (l, code) in list {
            if !is_success_code(code) {
                let msg = format!("[stores/s3-v1] Non-success code encountered while listing {}", code);
                eprintln!("{}", msg);
                warn!("{}", msg);
            }

            if l.name != self.storage.bucket {
                continue
            }

            // Loop through the results
            count += l.contents.len() as u32;
        }

        Ok(count)
    }

    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError> {
        // Access the bucket
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;
        debug!("retreiving keys from backup...");

        // Retrieve the items under the KV prefix
        let prefix = format!("/{}/{}", &self.name, KV_PREFIX_NAME);
        let listing = bucket.list_blocking(prefix.clone(), None)?;

        let mut keys = Vec::new();
        for (l, code) in listing {
            if !is_success_code(code) {
                let msg = format!("[stores/s3-v1] Non-success code encountered while listing {}", code);
                eprintln!("{}", msg);
                warn!("{}", msg);
            }

            if l.name != self.storage.bucket {
                continue
            }

            // Loop through the results
            for obj in l.contents {
                if let Some(key_name) = obj.key.split("/").last() {
                    keys.push(String::from(key_name));
                }
            }
        }

        // TODO: Check continutation tokens for keys > 50k?

        // Convert the listing into an interator that will contain just the keys
        Ok(Box::from(keys.into_iter()))
    }

    fn get_value(&self, key: RedisKey) -> Result<RedisBinaryValue, BackupError> {
        // Access the bucket
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;
        debug!("retreiving value from backup...");

        // Retrieve the items under the KV prefix
        let (data, code) = bucket.get_object_blocking(format!("/{}/{}/{}", &self.name, KV_PREFIX_NAME, &key))?;
        if !is_success_code(code) {
            if let Ok(resp) = String::from_utf8(data) {
                let msg = format!("[stores/s3-v1] Failed to retrieve value from S3: {}", resp);
                eprintln!("{}", msg);
                warn!("{}", msg);
                return Err(BackupError::ValueRetrievalFailed(key, format!("failed to retrieve value from S3: {}", resp)));
            } else {

                return Err(BackupError::ValueRetrievalFailed(key, format!("failed to retrieve value from S3")));
            }
        }

        Ok(data)
    }

    fn get_values(&self, keys: Box<dyn Iterator<Item=RedisKey>>) -> Result<Box<dyn Iterator<Item=(RedisKey, Result<RedisBinaryValue, BackupError>)>>, BackupError> {
        // Access the bucket
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;
        debug!("retrieving values from backup for given keys...");

        let mut values = Vec::new();
        // Get values for given keys from bucket sub-folder
        for k in keys {
            values.push((
                RedisKey::from(&k),
                bucket.get_object_blocking(format!("/{}/{}/{}", &self.name, KV_PREFIX_NAME, k))
                    .map(|(v, _)| v)
                    .map_err(|e| BackupError::ValueRetrievalFailed(String::from(&k), format!("failed to retrieve value: {}", e)))
            ));
        }


        Ok(Box::from(values.into_iter()))
    }

    fn write_pair(&self, key: RedisKey, value: RedisBinaryValue) -> Result<(), BackupError> {
        let bucket = Bucket::new(&self.storage.bucket, self.storage.region.clone(), self.storage.credentials.clone())?;
        debug!("writing value for key [{}]...", &key);

        // Write key & value to S3 bucket subfolder
        bucket.put_object_blocking(format!("/{}/{}/{}", self.name, KV_PREFIX_NAME, key), &value, "application/octet-stream")?;

        Ok(())
    }

    fn write_pairs(&self, pairs: Box<dyn Iterator<Item=(RedisKey, RedisBinaryValue)>>) -> Result<(), BackupError> {
        debug!("writing keys to backup...");

        // Write pairs to S3 bucket subfolder
        for (k, v) in pairs {
            self.write_pair(k, v)?;
        }

        Err(BackupError::NotImplemented)
    }
}
