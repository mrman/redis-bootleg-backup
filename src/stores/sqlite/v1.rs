use log::{debug, info, error};
use semver::Version;
use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;
use rusqlite::{Connection, NO_PARAMS, Error as RusqliteError, ToSql, Transaction};
use sha1::Sha1;

use crate::types::*;

/// Current version of the SQLiteStore
const VERSION: &'static str = "1.0.0";

/// Config for creating a restore from a given redis instance for a given folder on disk
/// like other custom executors/stores, this configuration must be read from environment
#[derive(Debug)]
pub struct SQLiteStoreOpts {
    /// Base directory which contains backups (SQLite DBs)
    pub base_dir: PathBuf,
}

impl SQLiteStoreOpts {
    /// Build opts from env
    fn from_env_map(env_map: &HashMap<String, String>) -> Result<SQLiteStoreOpts, BackupStoreError> {
        env_map.get("RBB_SQLITE_BASE_DIR")
            .map(PathBuf::from)
            .map(|base_dir| SQLiteStoreOpts { base_dir })
            .ok_or(BackupStoreError::InvalidConfiguration(
                String::from("missing required ENV variable RBB_SQLITE_BASE_DIR")
            ))
    }
}

/// SQLite backend
/// The SQLite Backend is a storage backend that writes and reads [SQLite](https://sqlite.org/) database files for backups
/// backups are stored as individual named files, and the key/value data is stored as data inside the database.
pub struct SQLiteStore {
    /// Version of the backend
    /// This version doesn't actually change, as versioning of the store matters a bit less for SQLite files
    /// Backups will be stored as loose files in the folder and that's generally enough
    version: Version,

    /// Options for the folder backend
    opts: SQLiteStoreOpts,
}

impl SQLiteStore {
    /// Create a new SQLiteStore, with options pulled from the provided environment mapping
    pub fn new(env_map: &HashMap<String, String>) -> Result<SQLiteStore, BackupStoreError> {
        return SQLiteStore::with_opts(SQLiteStoreOpts::from_env_map(env_map)?)
    }

    /// Create a SQLiteStore with explicit options
    pub fn with_opts(opts: SQLiteStoreOpts) -> Result<SQLiteStore, BackupStoreError> {
        let version = Version::parse(VERSION)
                .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to parse version: {}", e)))?;

        Ok(SQLiteStore {version, opts})
    }

    /// Retrieve the root path for the SQLite store, the folder that will hold the backups
    fn get_root_path(&self) -> Result<PathBuf, BackupStoreError> {
        return Ok(self.opts.base_dir.clone());
    }
}

impl BackupStore for SQLiteStore {
    /// Get the backup backend enum type
    fn get_type(&self) -> BackupStoreType {
        return BackupStoreType::SQLiteV1;
    }

    /// Get the version of the store itself
    fn get_store_version(&self) -> Result<Version, BackupStoreError> {
        debug!("[stores/sqlite-v1] reading version file...");
        Ok(self.version.clone())
    }

    /// Initialize the backend, ensuringf older is created and lockfile is made
    fn init(&mut self) -> Result<(), BackupStoreError> {
        debug!("[stores/sqlite-v1] initializing");

        // Ensure the root path folder exists
        let root_path = self.get_root_path()?;
        if !root_path.exists() {
            return Err(BackupStoreError::InvalidConfiguration(
                format!("root path [{}] not present", root_path.to_string_lossy())
            ));
        }

        // Ensure ensure version is present
        match self.get_store_version() {
            Ok(version) => {
                info!("[stores/sqlite-v1] detected version [{:?}]", version);
            },
            Err(e) => {
                let msg = format!("[stores/sqlite-v1] unexpected error during initialization {}", e);
                eprintln!("{}", msg);
                error!("{}", msg);
                let msg = format!("Unexpected error during init: {}", e);
                return Err(BackupStoreError::UnexpectedError(String::from(msg)));
            }
        };

        Ok(())
    }

    /// Cleanup the sqlite backend
    fn cleanup(&mut self) -> Result<(), BackupStoreError> {
        Ok(())
    }

    /// Get or create a backup by creating appropriate DB on disk
    fn get_or_create_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        let backup = SQLiteStoreBackup::create(self.version.clone(), self.get_root_path()?, backup_name)
            .map_err(|e| BackupStoreError::FailedToCreateBackup(format!("failed to create backup: {}", e)))?;
        Ok(Box::from(backup))
    }

    /// Get a backup
    fn get_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        // Retrieve the backup
        let backup = SQLiteStoreBackup::get(self.get_root_path()?, backup_name)
            .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to retrieve backup: {}", e)))?;

        Ok(Box::from(backup))
    }

}

/// SQLite backup
struct SQLiteStoreBackup {
    /// Version of the backup (should match the store at time of creation)
    version: Version,

    /// Name of the backup
    name: BackupName,

    /// Path to the sqlite DB on disk
    db_path: PathBuf,
}

impl From<RusqliteError> for BackupError {
    fn from(err: RusqliteError) -> BackupError {
        BackupError::UnexpectedError(format!("DB error: {}", err))
    }
}

/// Single-shot migration of all database schema
/// The first argument should be the *version* of the DB schema to which this applies
const SCHEMA_V1_0_0: &'static str = "
BEGIN;

CREATE TABLE version (
  version TEXT NOT NULL, -- version of the database (semver)
  schema_sha1 TEXT       -- SHA1 hash of the schema immediately after creation
);

-- Insert the initial value of the version table
INSERT INTO version (version) VALUES ('1.0.0');

-- Add trigger to ensure that version can never be inserted into again
CREATE TRIGGER version_no_insert
BEFORE INSERT ON version
BEGIN
  SELECT RAISE(FAIL, 'only one version row allowed');
END;

-- Table that stores KV data
CREATE TABLE kv (
  key TEXT NOT NULL,   -- redis key
  value BLOG NOT NULL, -- redis value
  ttl_seconds INTEGER,  -- TTL
  inserted_at TEXT DEFAULT (DATETIME('now'))  -- insert time
);

END;
";

impl SQLiteStoreBackup {
    fn get(base_dir: PathBuf, name: BackupName) -> Result<SQLiteStoreBackup, BackupError> {
        // Create the backup
        let db_path = base_dir.join(format!("{}.sqlite", name));

        // Read the version from the DB
        let conn = Connection::open(&db_path)?;
        let version = SQLiteStoreBackup::read_version_from_db(&conn)?;

        let mut backup = SQLiteStoreBackup{version, name, db_path};

        // Initialize the backup
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }


    fn create(version: Version, base_dir: PathBuf, name: BackupName) -> Result<SQLiteStoreBackup, BackupError> {
        // Create the backup
        let db_path = base_dir.join(format!("{}.sqlite", name));
        let mut backup = SQLiteStoreBackup{version, name, db_path};

        // Initialize the backup
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }

    /// Initialize a given backup
    fn init(&mut self) -> Result<&mut Self, BackupError> {
        // Check if it already has the version file (so already initialized)
        if self.db_path.clone().exists() {
            // Ensure the schema is as we expect
            self.check_schema()?;

            // Check that the version matches
            // in the future this might require keeping track of supported versions
            self.check_version()?;

            return Ok(self);
        }

        // Create the database file
        fs::File::create(&self.db_path)
            .map_err(|e| BackupError::UnexpectedError(format!("failed to create database file: {}", e)))?;

        // Set up the DB schema
        let _ = self.setup_schema()?;

        Ok(self)
    }

    /// Connect to the db
    fn connect(&self) -> Result<Connection, BackupError> {
        // Connect to the DB, save the connection
        Connection::open(&self.db_path)
            .map_err(|e| BackupError::ConnectionFailure(format!("Failed to connect to DB @ [{}]: {:?}", &self.db_path.to_string_lossy(), e)))
    }

    /// Read a version from a db
    fn read_version_from_db(conn: &Connection) -> Result<Version, BackupError> {
        // Retrieve the sqlite_store_version pragma
        let raw: String = conn.query_row(
            "SELECT version FROM version LIMIT 1",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        Version::parse(raw.as_str())
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to parse DB version value:, {}", e)))
    }

    /// Retrieve the version of the DB
    fn get_version_from_db(&self) -> Result<Version, BackupError> {
        let conn = self.connect()?;
        let version = SQLiteStoreBackup::read_version_from_db(&conn)?;
        Ok(version)
    }

    /// Set up the database schema
    fn setup_schema(&self) -> Result<(), BackupError> {
        let conn = self.connect()?;

        // Set up the schema
        conn.execute_batch(SCHEMA_V1_0_0)?;

        // Get all the SQL for the current schema
        let schema: String = conn.query_row(
            "SELECT GROUP_CONCAT(sql) FROM sqlite_master",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        // Hash the current schema
        let hashed = Sha1::from(schema.as_bytes());

        // Save the current hashed schema to the DB
        // The version table should be a table with a single row
        conn.execute(
            "UPDATE version SET schema_sha1=?",
            &[hashed.hexdigest()],
        )?;

        Ok(())
    }

    /// Ensure that the version of the DB matches the in-memory version
    fn check_version(&self) -> Result<(), BackupError> {
        // Ensure version in DB matches version of store
        // in the future this will likely need to be replaced by a "supports" check
        let version = self.get_version_from_db()?;
        if version != self.version {
            return Err(BackupError::UnsupportedVersion(format!("Version mismatch, SQLiteStore version [{}] does not support backups of version [{}]", self.version, version)))
        }

        Ok(())
    }

    /// Calculate the current DB schema's SHA1 hash
    fn calculate_schema_hash(&mut self) -> Result<String, BackupError> {
        let conn = self.connect()?;

        // Get all the SQL for the current schema
        let schema: String = conn.query_row(
            "SELECT GROUP_CONCAT(sql) FROM sqlite_master",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        // Hash the current schema
        Ok(Sha1::from(schema.as_bytes()).hexdigest())
    }

    /// Retrieve the schema hash for a given database
    fn get_schema_hash(&mut self) -> Result<String, BackupError> {
        let conn = self.connect()?;

        let schema: String = conn.query_row(
            "SELECT schema_sha1 FROM version",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        Ok(schema)
    }

    /// Check that the DB schema is in place as we expect
    fn check_schema(&mut self) -> Result<(), BackupError> {
        self.connect()?;

        let calculated = self.calculate_schema_hash()?;
        let retrieved = self.get_schema_hash()?;

        if calculated != retrieved {
            return Err(BackupError::CorruptedBackup(String::from("schema hashes do not match, database structure has changed")));
        }

        Ok(())
    }

    /// Helper function for writing a single key given an existing connection
    fn write_single_kv_pair_in_transaction(
        &self,
        tx: &Transaction,
        key: &RedisKey,
        value: &RedisBinaryValue,
    ) -> Result<(), BackupError> {
        let result = tx.execute(
            "INSERT INTO kv (key, value) VALUES (?,?);",
            &[key as &dyn ToSql, value],
        )?;

        if result == 0 {
            return Err(BackupError::KeyBackupFailed(String::from(key), String::from("Failed to write key, no rows written")))
        }

        Ok(())
    }
}

impl Backup for SQLiteStoreBackup {
    fn get_store_type(&self) -> BackupStoreType {
        return BackupStoreType::SQLiteV1;
    }

    fn get_version(&self) -> Result<Version, BackupError> {
        self.get_version_from_db()
    }

    fn get_name(&self) -> BackupName {
        self.name.clone()
    }

    fn get_key_count(&self) -> Result<u32, BackupError> {
        let conn = self.connect()?;

        let count: u32 = conn.query_row(
            "SELECT COUNT(*) FROM kv",
            NO_PARAMS,
            |row| row.get(0),
        )?;

        Ok(count)
    }

    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError> {
        let conn = self.connect()?;
        debug!("retreiving keys from backup...");

        let mut stmt = conn.prepare("SELECT key FROM kv")?;
        let keys: Vec<String> = stmt.query_map(NO_PARAMS, |row| row.get(0))?
            .filter(|maybe_row| maybe_row.is_ok())
            .map(|maybe_row| maybe_row.unwrap())
            .collect();

        Ok(Box::from(keys.into_iter()))
    }

    fn get_value(&self, key: RedisKey) -> Result<RedisBinaryValue, BackupError> {
        let conn = self.connect()?;
        debug!("retreiving value from backup...");

        conn.query_row(
            "SELECT value FROM kv WHERE key = ?",
            &[&key],
            |row| row.get(0),
        )
            .map_err(|e| {
                match e {
                    RusqliteError::QueryReturnedNoRows => BackupError::ValueRetrievalFailed(key, format!("key not present")),
                    _ => e.into()
                }
            })
    }

    fn get_values(&self, keys: Box<dyn Iterator<Item=RedisKey>>) -> Result<Box<dyn Iterator<Item=(RedisKey, Result<RedisBinaryValue, BackupError>)>>, BackupError> {
        let conn = self.connect()?;
        debug!("retrieving values from backup for given keys...");

        let mut values = Vec::new();

        for k in keys {
            let value = conn.query_row(
                "SELECT value FROM kv WHERE key = ?",
                &[&k],
                |row| row.get(0),
            )
                .map_err(|e| {
                    match e {
                        RusqliteError::QueryReturnedNoRows => BackupError::ValueRetrievalFailed(String::from(&k), format!("key not present")),
                        _ => e.into()
                    }
                });

            values.push((k, value));
        }

        Ok(Box::from(values.into_iter()))
    }

    fn write_pair(&self, key: RedisKey, value: RedisBinaryValue) -> Result<(), BackupError> {
        let conn = self.connect()?;
        debug!("writing value for key [{}]...", &key);

        let result = conn.execute(
            "INSERT INTO kv (key, value) VALUES (?,?);",
            &[&key as &dyn ToSql, &value],
        )?;

        if result == 0 {
            return Err(BackupError::KeyBackupFailed(String::from(&key), String::from("Failed to write key, no rows written")))
        }

        Ok(())
    }

    fn write_pairs(&self, pairs: Box<dyn Iterator<Item=(RedisKey, RedisBinaryValue)>>) -> Result<(), BackupError> {
        let mut conn = self.connect()?;
        debug!("writing keys to backup...");
        // Write all the pairs inside a transaction

        let tx = conn.transaction()?;

        for (key, value) in pairs {
            self.write_single_kv_pair_in_transaction(&tx, &key, &value)?;
        }

        tx.commit()?;

        Ok(())
    }
}

mod tests {
    #[cfg(test)]
    use super::*;
    #[cfg(test)]
    use uuid::Uuid;


    // Helper for creating a sqlite backend with a temp folder
    #[cfg(test)]
    fn make_test_store() -> Result<SQLiteStore, String> {
        // Create a temp folder path
        let uuid = Uuid::new_v4().to_simple().to_string();
        let base_dir = std::env::temp_dir().join("redis-bootleg-backup-test").join(uuid);

        // Create the temp folder and set it as the base dir
        fs::create_dir_all(&base_dir)
            .map_err(|_| String::from("Failed to create temp dir for test"))?;

        Ok(SQLiteStore::with_opts(SQLiteStoreOpts { base_dir })?)
    }

    /// get_store_version() for SQLite store passes even though init has not been called
    /// because the version for SQLite is tied to the executable and should be backwards compatible
    /// individual backup schema versions are managed by checking the schema
    #[test]
    fn test_get_store_version_before_init() -> Result<(), String> {
        let base_dir = std::env::temp_dir();
        let backend = SQLiteStore::with_opts(SQLiteStoreOpts{ base_dir })?;

        if let Err(_) = backend.get_store_version() {
            return Err(String::from("version should be knowable without init"));
        }

        Ok(())
    }

    /// get_store_version() shouldn't fail should fail on a folder with no version file
    #[test]
    fn test_get_store_version_for_backend_without_version() -> Result<(), String> {
        let backend = make_test_store()?;
        let _ = backend.get_store_version()?;
        Ok(())
    }

    /// init() doesn't fail
    #[test]
    fn test_init_generates_db_file() -> Result<(), String> {
        let mut backend = make_test_store()?;

        // Initialize the backend
        backend.init()?;

        Ok(())
    }

    // get_or_create() to create a backup (check version as well)
    #[test]
    fn test_goc_works() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        let backup = store.get_or_create_backup(BackupName::from("test-backup"))?;
        assert_eq!(backup.get_version()?, store.get_store_version()?, "backup version matches store");

        store.cleanup()?;

        Ok(())
    }

    // get_or_create() an for existing backup shouldn't create new folders or fail
    #[test]
    fn test_goc_existing_backup() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create first backup
        let first = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p).into_iter());
        let _ = first.write_pairs(pairs)?;
        assert_eq!(first.get_key_count()?, 1, "first store contains the inserted key");

        // Create a second backup
        let second = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Key counts for the first and second should match
        assert_eq!(first.get_key_count()?, second.get_key_count()?, "key counts match for both backups");

        store.cleanup()?;

        Ok(())
    }

    /// reading keys should work
    #[test]
    fn test_read_written_keys() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p).into_iter());
        let _ = backup.write_pairs(pairs)?;
        assert_eq!(backup.get_key_count()?, 1, "backup contains the inserted key");

        // Read the keys written to the backup
        let mut written_keys = backup.get_keys()?;
        let first_key = written_keys.next().unwrap();

        // Ensure key that was written was retrieved
        assert_eq!(first_key, RedisKey::from("test-key"), "first written key is accurate");

        store.cleanup()?;

        Ok(())
    }

    /// values that were backed up should be retrievable
    #[test]
    fn test_read_written_values() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let key = RedisKey::from("test-key");
        let p = (key.clone(), vec![0u8,1u8,2u8,3u8, 4u8, 5u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;
        assert_eq!(backup.get_key_count()?, 1, "backup contains the inserted key");

        // Read the keys & values written to the backup
        let keys = vec!(key).into_iter();
        let mut pairs = backup.get_values(Box::new(keys))?;
        let (key, maybe_val) = pairs.next().unwrap();

        // Ensure the key for the pari that was written is correct
        assert_eq!(key, p.0, "key matches");
        // Ensure that the value was returned and is correct
        assert_eq!(maybe_val, Ok(p.1), "value matches");

        store.cleanup()?;

        Ok(())
    }

    // get_or_create() on an existing backup with bad version data should fail
    #[test]
    fn test_goc_corrupted_version() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;

        // Use what we know about the internals of the storage to access the SQLite databsae, and modify it,
        //  changing the version to an invalid value
        let expected_db_path = store.get_root_path()?.join("test-backup.sqlite");
        assert!(expected_db_path.exists(), "DB file exists");
        let conn = Connection::open(expected_db_path)
            .map_err(|e| format!("failed to open connection: {}", e))?;
        conn.execute("UPDATE version SET version='NOPE'", NO_PARAMS)
            .map_err(|e| format!("failed to update version to invalid value: {}", e))?;

        // Attempting to get_or_create (the existing backup)
        match store.get_or_create_backup(backup_name) {
            Err(BackupStoreError::FailedToCreateBackup(reason)) => {
                assert!(reason.contains("Failed to parse DB version value"));
                store.cleanup()?;
                Ok(())
            },
            Err(e) => Err(format!("wrong error: {}", e)),
            Ok(_) => Err(String::from("init should have failed with an invalid version file"))
        }
    }
}
