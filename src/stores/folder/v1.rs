use fslock::LockFile;
use log::{debug, info, error};
use semver::Version;
use std::collections::HashMap;
use std::fs;
use std::fs::{OpenOptions, File};
use std::io::{BufReader, Write, BufRead};
use std::path::PathBuf;
use std::collections::BTreeSet;

use crate::types::*;

/// Current version of the FolderStore
const VERSION: &'static str = "1.0.0";

const VERSION_FILE_NAME: &'static str = ".version";
const KEY_LISTING_FILE_NAME: &'static str = ".keys";
const LOCK_FILE_NAME: &'static str = ".lock";
const BACKUPS_FOLDER_NAME: &'static str = "backups";
const FOLDER_BACKUP_KEYS_SUBFOLDER_NAME: &'static str = "kv";

/// Config for creating a restore from a given redis instance for a given folder on disk
/// like other custom executors/stores, this configuration must be read from environment
#[derive(Debug)]
pub struct FolderStoreOpts {
    /// Base directory which contains metadata, backups, and restores
    pub base_dir: PathBuf,
}

impl FolderStoreOpts {
    /// Build opts from env
    fn from_env_map(env_map: &HashMap<String, String>) -> Result<FolderStoreOpts, BackupStoreError> {
        env_map.get("RBB_FOLDER_BASE_DIR")
            .map(PathBuf::from)
            .map(|base_dir| FolderStoreOpts { base_dir })
            .ok_or(BackupStoreError::InvalidConfiguration(
                String::from("missing required ENV variable RBB_FOLDER_BASE_DIR")
            ))
    }
}

/// Folder backend
pub struct FolderStore {
    /// Version of the backend
    version: Version,

    /// Options for the folder backend
    opts: FolderStoreOpts,

    /// Lock file for performing operations
    lockfile: Option<LockFile>,
}

impl FolderStore {
    pub fn new(env_map: &HashMap<String, String>) -> Result<FolderStore, BackupStoreError> {
        let version = Version::parse(VERSION)
                .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to parse version: {}", e)))?;
        let opts = FolderStoreOpts::from_env_map(env_map)?;

        Ok(FolderStore {
            version,
            opts,
            lockfile: None,
        })
    }

    pub fn with_opts(opts: FolderStoreOpts) -> Result<FolderStore, BackupStoreError> {
        let version = Version::parse(VERSION)
                .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to parse version: {}", e)))?;

        Ok(FolderStore {
            version,
            opts,
            lockfile: None,
        })
    }

    /// Retrieve the root path
    fn get_root_path(&self) -> Result<PathBuf, BackupStoreError> {
        return Ok(self.opts.base_dir.clone());
    }

    /// Retrieve the version path
    fn get_version_file_path(&self) -> Result<PathBuf, BackupStoreError> {
        return Ok(self.opts.base_dir.clone().join(VERSION_FILE_NAME));
    }

    /// Retrieve the lockfile path
    fn get_lockfile_path(&self) -> Result<PathBuf, BackupStoreError> {
        return Ok(self.opts.base_dir.clone().join(LOCK_FILE_NAME));
    }

    /// Retrieve the backups path
    fn get_backups_folder_path(&self) -> Result<PathBuf, BackupStoreError> {
        return Ok(self.opts.base_dir.clone().join(BACKUPS_FOLDER_NAME));
    }

    /// Create the folder that will hold the backups
    fn create_backups_folder(&self) -> Result<(), BackupStoreError> {
        let path = self.get_backups_folder_path()?;

        // If the path already exists quit early
        if path.exists() { return Ok(()); }

        fs::create_dir(&path)
            .map_err(|_| BackupStoreError::UnexpectedError(
                format!("Failed to create backups dir @ [{}]", &path.to_string_lossy()),
            ))
    }

    /// Write out the version file
    fn write_version_file(&self) -> Result<(), BackupStoreError> {
        fs::write(self.get_version_file_path()?, self.version.to_string().as_bytes())
            .map_err(|_| BackupStoreError::UnexpectedError(String::from("Failed to write version file")))
    }
}

impl BackupStore for FolderStore {
    /// Get the backup backend enum type
    fn get_type(&self) -> BackupStoreType {
        return BackupStoreType::FolderV1;
    }

    /// Get the version of the store itself
    fn get_store_version(&self) -> Result<Version, BackupStoreError> {
        debug!("[stores/folder-v1] reading version file...");
        let version_file_path = self.get_version_file_path()?;

        // If version file doesn't exist or can't be read
        let raw = fs::read_to_string(&version_file_path)
            .map_err(|_| BackupStoreError::UninitializedStore(
                format!("failed to read version file @ [{}]", version_file_path.to_string_lossy()),
            ))?;

        // Attempt to parse the version into semver
        debug!("[stores/folder-v1] parsing version contents...");
        Version::parse(raw.as_str())
            .map_err(|e| BackupStoreError::InvalidConfiguration(format!("version file contents invalid, error: {}", e)))
    }

    /// Initialize the backend, ensuringf older is created and lockfile is made
    fn init(&mut self) -> Result<(), BackupStoreError> {
        debug!("[stores/folder-v1] initializing");

        // Ensure the backups folder exists
        let root_path = self.get_root_path()?;
        if !root_path.exists() {
            return Err(BackupStoreError::InvalidConfiguration(
                format!("root path [{}] not present", root_path.to_string_lossy())
            ));
        }

        // Start setting up lockfile, open & obtain it
        let lockfile_path = self.get_lockfile_path()?;
        debug!("[stores/folder-v1] setting up lock file... [{}]", lockfile_path.to_string_lossy());

        let lockfile = LockFile::open(&lockfile_path)
            .map_err(|_| BackupStoreError::UnexpectedError(String::from("Failed to create lockfile")))?;
        self.lockfile = Some(lockfile);
        if let Some(f) = &mut self.lockfile {
            f.lock()
            .map_err(|_| BackupStoreError::UnexpectedError(String::from("Failed to obtain lock on lockfile")))?;
        }

        // Ensure version matches
        match self.get_store_version() {
            // If a version is present ensure it matches the store version we're using
            Ok(version) => {
                info!("[stores/folder-v1] detected version [{:?}]", version);
                if version != self.version {
                    let msg = format!("Version mismatch, folder is for version [{:?}], but current version is [{:?}]", version, self.version);
                    return Err(BackupStoreError::InvalidConfiguration(String::from(msg)));
                }
            },
            // Uninitialized store means we have a change to initialize it
            Err(BackupStoreError::UninitializedStore(_)) => {
                info!("[stores/folder-v1] detected uninitialized store, initializing...");

                // Write the version file
                self.write_version_file()?;

                // Create backups directory
                self.create_backups_folder()?;
            },
            // All other errors are unexpected
            Err(e) => {
                let msg = format!("[stores/folder-v1] unexpected error during initialization {}", e);
                eprintln!("{}", msg);
                error!("{}", msg);
                let msg = format!("Unexpected error during init: {}", e);
                return Err(BackupStoreError::UnexpectedError(String::from(msg)));
            }
        };

        Ok(())
    }

    /// Cleanup the folder backend, removing lock file
    fn cleanup(&mut self) -> Result<(), BackupStoreError> {
        // Unlock & remove lockfile
        if let Some(f) = &mut self.lockfile {
            f.unlock()
            .map_err(|_| BackupStoreError::UnexpectedError(String::from("Failed to unlock lockfile")))?;

            let _ = fs::remove_file(self.get_lockfile_path()?);
        }

        Ok(())
    }

    /// Get or create a backup by creating appropriate folders & files on disk
    fn get_or_create_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        let backup = FolderStoreBackup::create(self.version.clone(), self.get_backups_folder_path()?, backup_name)
            .map_err(|e| BackupStoreError::FailedToCreateBackup(format!("failed to create backup: {}", e)))?;
        Ok(Box::from(backup))
    }

    /// Get a backup
    fn get_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError> {
        // Retrieve the backup
        let backup = FolderStoreBackup::get(self.get_backups_folder_path()?, backup_name)
            .map_err(|e| BackupStoreError::UnexpectedError(format!("failed to retrieve backup: {}", e)))?;

        Ok(Box::from(backup))
    }

}

/// Folder backup
struct FolderStoreBackup {
    /// Version of the backup (should match the store at time of creation)
    version: Version,

    /// Name of the backup
    name: BackupName,

    /// Base directory of the backup on disk
    base_dir: PathBuf,
}

impl FolderStoreBackup {
    fn get(root_path: PathBuf, name: BackupName) -> Result<FolderStoreBackup, BackupError> {
        // Derive base directory for this individual backup
        let base_dir = root_path.join(&name);

        // Return early if the path already exists (lockfiles determine overlap for operations)
        if !base_dir.exists() {
            fs::create_dir(&base_dir)
                .map_err(|e| BackupError::UnexpectedError(
                    format!("Failed to create new backup @ [{}] : {}", &base_dir.to_string_lossy(), e),
                ))?;
        }

        // Read version from existing backup
        let version_file_path = base_dir.join(VERSION_FILE_NAME);

        // If version file doesn't exist or can't be read
            let raw = fs::read_to_string(&version_file_path)
            .map_err(|_| BackupError::CorruptedBackup(
                format!("failed to read version file @ [{}]", version_file_path.to_string_lossy()),
            ))?;

        let version = Version::parse(raw.as_str())
            .map_err(|e| BackupError::CorruptedBackup(format!("version file contents invalid, error: {}", e)))?;

        // Read the version from the backup path
        let mut backup = FolderStoreBackup{version, name, base_dir};
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }

    fn create(version: Version, root_path: PathBuf, name: BackupName) -> Result<FolderStoreBackup, BackupError> {
        // Derive base directory for this individual backup
        let base_dir = root_path.join(&name);

        // Return early if the path already exists (lockfiles determine overlap for operations)
        if !base_dir.exists() {
            fs::create_dir(&base_dir)
                .map_err(|e| BackupError::UnexpectedError(
                    format!("Failed to create new backup @ [{}] : {}", &base_dir.to_string_lossy(), e),
                ))?;
        }

        // Create and initialize the backup
        let mut backup = FolderStoreBackup{version, name, base_dir};
        backup.init()
            .map_err(|e| BackupError::UnexpectedError(format!("failed to initialize backup: {}", e)))?;

        Ok(backup)
    }

    /// Initialize a given backup
    fn init(&mut self) -> Result<&mut Self, BackupError> {
        // Check if it already has the version file (so already initialized)
        if self.get_version_file_path()?.exists() {
            self.get_version()?;
            self.check_key_dump_dir()?;
            return Ok(self);
        }

        // Write a new verison file
        let _ = self.write_version_file()?;

        // Create the subdirectory that will hold key value data if it doesn't exist
        let key_dump_dir = self.get_key_dump_dir()?;
        if !key_dump_dir.exists() {
            fs::create_dir(key_dump_dir)
                .map_err(|e| BackupError::UnexpectedError(
                    format!("Failed to create key dump subdir for backup: {}", e),
                ))?;
        }

        // Write the key listing file
        let _ = self.write_key_listing_file()?;

        Ok(self)
    }

    fn get_file_count(&self) -> Result<u32, BackupError> {
        let path = self.get_key_dump_dir()?;
        let count = fs::read_dir(path)
            .map_err(|_| BackupError::UnexpectedError(
                format!("Failed to count files in dir [{}]", self.base_dir.to_string_lossy())
            ))?
            .count();

        Ok(count as u32)
    }

    /// Get the path to where the values for keys are dumped
    fn get_key_dump_dir(&self) -> Result<PathBuf, BackupError> {
        Ok(self.base_dir.clone().join(FOLDER_BACKUP_KEYS_SUBFOLDER_NAME))
    }

    /// Get the path to the version file
    fn get_version_file_path(&self) -> Result<PathBuf, BackupError> {
        Ok(self.base_dir.clone().join(VERSION_FILE_NAME))
    }

    /// Write out the version file
    fn write_version_file(&self) -> Result<(), BackupError> {
        fs::write(self.get_version_file_path()?, self.version.to_string().as_bytes())
            .map_err(|_| BackupError::UnexpectedError(String::from("Failed to write backup version file")))
    }

    /// Write out the key listing file (if not present)
    fn write_key_listing_file(&self) -> Result<(), BackupError> {
        let path = self.get_key_listing_file_path()?;
        if path.exists() {
            return Ok(());
        }

        // Create the file
        File::create(path)
            .map_err(|_| BackupError::UnexpectedError(String::from("Failed to write version file")))?;

        self.clean_key_metadata_file()?;

        Ok(())
    }

    fn check_key_dump_dir(&self) -> Result<(), BackupError> {
        let path = self.get_key_dump_dir()?;
        if path.exists() {
            return Ok(());
        }
        Err(BackupError::UnexpectedError(String::from("key dump directory is missing/inaccessible")))
    }

    /// Get the path to the key listing file
    fn get_key_listing_file_path(&self) -> Result<PathBuf, BackupError> {
        Ok(self.base_dir.clone().join(KEY_LISTING_FILE_NAME))
    }

    /// Update the key metadata file for a given backup
    fn update_key_metadata_file(&self, key: RedisKey) -> Result<(), BackupError> {
        let path = self.get_key_listing_file_path()?;
        let mut file = OpenOptions::new().append(true).open(path)
            .map_err(|_| BackupError::UnexpectedError(String::from("Failed to open key listing file")))?;

        file.write_all(format!("{}\n",key).as_bytes())
            .map_err(|_| BackupError::UnexpectedError(String::from("Failed to update key listing file")))
    }

    /// clean the key metadata file for a given backup
    fn clean_key_metadata_file(&self) -> Result<(), BackupError> {
        // Open the keys metadata file
        let path = self.get_key_listing_file_path()?;
        let key_listing = File::open(&path)
            .map_err(|e| BackupError::UnexpectedError(format!("failed to open keys listing file: {}", e)))?;

        // Read the lines out of the file into a hash set
        let key_set: BTreeSet<RedisKey> = BufReader::new(key_listing)
            .lines()
            .into_iter()
            .filter(|maybe_line| maybe_line.is_ok())
            .map(|l| String::from(l.unwrap().trim()))
            .collect();

        // Get new path, ensure it doesn't collide with the old
        let mut new_path = path.clone();
        new_path.set_extension("new");
        if path == new_path {
            return Err(BackupError::UnexpectedError(String::from("failed to rename path for keys listing file")));
        }

        // Create the new file to write the key hash into it
        let mut new_key_listing = File::create(&new_path)
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to open key listing file: {:?}", e)))?;

        // Write the deduped keys back out
        key_set
            .into_iter()
            .try_for_each(|k| new_key_listing.write_all(format!("{}\n",k).as_bytes()))
            .map_err(|e| BackupError::UnexpectedError(format!("Failed midway writing new key listing file: {:?}", e)))?;

        // Rename the files
        println!("rename [{}] -> [{}]", new_path.to_string_lossy(), path.to_string_lossy());
        fs::rename(&new_path, &path)
            .map_err(|e| BackupError::UnexpectedError(format!("Failed to replace key listing file: {:?}", e)))?;

        Ok(())
    }

}

impl Backup for FolderStoreBackup {
    fn get_store_type(&self) -> BackupStoreType {
        return BackupStoreType::FolderV1;
    }

    fn get_version(&self) -> Result<Version, BackupError> {
        let version_file_path = self.get_version_file_path()?;
        let raw = fs::read_to_string(&version_file_path)
            .map_err(|_| BackupError::UnexpectedError(
                format!("failed to read backup version file @ [{}]", version_file_path.to_string_lossy()),
            ))?;

        // Attempt to parse the version into semver
        Version::parse(raw.as_str())
            .map_err(|e| BackupError::UnexpectedError(format!("version file contents invalid, error: {}", e)))
    }

    fn get_name(&self) -> BackupName {
        self.name.clone()
    }

    fn get_key_count(&self) -> Result<u32, BackupError> {
        self.get_file_count()
    }

    /// Get keys for a folder backup, since every file is a redis key, this is synonymous with a 1 level dir listing
    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError> {
        debug!("retreiving keys from backup...");

        // Open the key listing file that should be stored alongside the backup
        let path = self.get_key_listing_file_path()?;
        let key_listing = File::open(path)
            .map_err(|e| BackupError::UnexpectedError(format!("failed to open keys listing file: {}", e)))?;

        // Read the lines out of the file
        let lines = BufReader::new(key_listing)
            .lines()
            .into_iter()
            .filter(|maybe_line| maybe_line.is_ok())
            .map(|l| String::from(l.unwrap().trim()));

        Ok(Box::from(lines))
    }

    fn get_value(&self, key: RedisKey) -> Result<RedisBinaryValue, BackupError> {
        // Build path to the file
        let dir = self.get_key_dump_dir()?;
        let path = dir.join(key.as_str());

        let value = fs::read(path.clone())
            .map_err(|_| BackupError::ValueRetrievalFailed(key, format!("Failed to read file [{}]", path.to_string_lossy())))?;

        return Ok(value)
    }

    fn get_values(&self, keys: Box<dyn Iterator<Item=RedisKey>>) -> Result<Box<dyn Iterator<Item=(RedisKey, Result<RedisBinaryValue, BackupError>)>>, BackupError> {
        debug!("retrieving values from backup for given keys...");
        // Prefix the keys which should be filenames with paths
        let dir = self.get_key_dump_dir()?;
        let pairs = keys
            .map(move |k| {
                // Build a tuple of the string and the read file path
                let path = dir.join(k.clone());
                return (
                    String::from(k.as_str()),
                    fs::read(path.clone())
                        .map_err(|_| BackupError::ValueRetrievalFailed(k, format!("Failed to read file [{}]", path.to_string_lossy())))
                );
            });

        Ok(Box::from(pairs))
    }

    fn write_pair(&self, key: RedisKey, value: RedisBinaryValue) -> Result<(), BackupError> {
        let path = self.get_key_dump_dir()?;
        let key_path = path.join(&key);

        debug!("writing value for key [{}]...", &key);
        fs::write(&key_path, value)
            .map_err(|e| BackupError::KeyBackupFailed(
                String::from(key.as_str()),
                format!("failed to write key [{}] to path [{}]: {}", key, key_path.to_string_lossy(), e),
            ))?;

        // Write to the key metadata file
        self.update_key_metadata_file(key)?;

        Ok(())
    }

    fn write_pairs(&self, mut pairs: Box<dyn Iterator<Item=(RedisKey, RedisBinaryValue)>>) -> Result<(), BackupError> {
        debug!("writing keys to backup...");

        // Write all keys to the file
        pairs.try_for_each(|(k, v)| self.write_pair(k, v))?;

        // Filter the key metadata file of all the
        self.clean_key_metadata_file()?;


        Ok(())
    }
}

mod tests {
    #[cfg(test)]
    use super::*;
    #[cfg(test)]
    use uuid::Uuid;

    // Helper for creating a folder backend with a test folder
    #[cfg(test)]
    fn make_test_store() -> Result<FolderStore, String> {
        // Create a temp folder path
        let uuid = Uuid::new_v4().to_simple().to_string();
        let base_dir = std::env::temp_dir().join("redis-bootleg-backup-test").join(uuid);

        // Create the folder and set it as the base dir
        fs::create_dir_all(&base_dir)
            .map_err(|_| String::from("Failed to create temp dir for test"))?;

        Ok(FolderStore::with_opts(FolderStoreOpts { base_dir })?)
    }

    // Helper for creating a folder backend with a test folder
    // NOTE: do not use this if you're doing a normal init
    #[cfg(test)]
    fn make_partially_initialized_test_store(version_str: &str) -> Result<FolderStore, String> {
        let version = Version::parse(version_str).map_err(|_| String::from("failed to make version"))?;
        let backend = make_test_store()?;

        // Write the version to the file
        let path = backend.get_version_file_path()?;
        fs::write(path, version.to_string().as_bytes()).map_err(|_| String::from("failed to write version file"))?;

        Ok(backend)
    }

    /// get_store_version() should fail when called before init
    #[test]
    fn test_get_store_version_before_init() -> Result<(), String> {
        let base_dir = std::env::temp_dir();
        let backend = FolderStore::with_opts(FolderStoreOpts{ base_dir })?;

        match backend.get_store_version() {
            Err(BackupStoreError::UninitializedStore(_)) => Ok(()),
            _ => Err(String::from("expected version to return uninitialized store error")),

        }
    }

    /// get_store_version() should fail on a folder with no version file
    #[test]
    fn test_get_store_version_for_backend_without_version() -> Result<(), String> {
        let backend = make_test_store()?;

        match backend.get_store_version() {
            Err(BackupStoreError::UninitializedStore(reason)) => {
                assert!(reason.contains("failed to read version file"));
                Ok(())
            }
            other => Err(format!("unexpected result of get_store_version(): {:?}", other)),
        }
    }

    /// get_store_version() should fail on a version file with non-semver content
    #[test]
    fn test_get_store_version_for_backend_with_invalid_version() -> Result<(), String> {
        let backend = make_test_store()?;

        // Write invalid version data
        let path = backend.get_version_file_path()?;
        fs::write(path, "NOPE").map_err(|_| String::from("failed to write version file"))?;

        // Ensure getting the version return the expected error
        match backend.get_store_version() {
            Err(BackupStoreError::InvalidConfiguration(reason)) => {
                assert!(reason.contains("version file contents invalid"));
                Ok(())
            },
            other => Err(format!("unexpected result of get_store_version(); {:?}", other))
        }
    }

    /// get_store_version() should work on a backend initialized to a folder with a valid version file
    #[test]
    fn test_get_store_version_for_backend_with_valid_version() -> Result<(), String> {
        let version = Version::parse(VERSION).map_err(|_| String::from("failed to make version"))?;
        let backend = make_partially_initialized_test_store(VERSION)?;

        // Ensure getting the version return the expected error
        match backend.get_store_version() {
            Ok(v) => {
                assert_eq!(version, v);
                Ok(())
            },
            other => Err(format!("unexpected result of get_store_version(); {:?}", other))
        }
    }

    /// init() generates lockfile
    #[test]
    fn test_init_generates_lockfile() -> Result<(), String> {
        let mut backend = make_partially_initialized_test_store(VERSION)?;

        // Initialize the backend
        backend.init()?;

        // Ensure that the lockfile is present
        let path = backend.get_lockfile_path()?;
        assert!(path.exists(), "path exists");

        Ok(())
    }

    /// cleanup() removes lockfile
    #[test]
    fn test_cleanup_removes_lockfile() -> Result<(), String> {
        let mut backend = make_partially_initialized_test_store(VERSION)?;

        // Initialize the backend
        backend.init()?;

        // Ensure that the lockfile is present
        let path = backend.get_lockfile_path()?;
        assert!(path.exists(), "path exists");

        let _ = backend.cleanup()?;

        // Ensure that the lockfile is present
        assert!(!path.exists(), "lockfile removed was removed");

        Ok(())
    }

    /// get_or_create() to create a backup (check version as well)
    #[test]
    fn test_goc_works() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        let backup = store.get_or_create_backup(BackupName::from("test-backup"))?;
        assert_eq!(backup.get_version()?, store.get_store_version()?, "backup version matches store");

        store.cleanup()?;

        Ok(())
    }

    /// get_or_create() an for existing backup shouldn't create new folders or fail
    #[test]
    fn test_goc_existing_backup() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create first backup
        let first = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p).into_iter());
        let _ = first.write_pairs(pairs)?;
        assert_eq!(first.get_key_count()?, 1, "first store contains the inserted key");

        // Create a second backup
        let second = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Key counts for the first and second should match
        assert_eq!(first.get_key_count()?, second.get_key_count()?, "key counts match for both backups");

        store.cleanup()?;

        Ok(())
    }

    /// reading keys should work
    #[test]
    fn test_read_written_keys() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup = store.get_or_create_backup(BackupName::from("test-backup"))?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p).into_iter());
        let _ = backup.write_pairs(pairs)?;
        assert_eq!(backup.get_key_count()?, 1, "backup contains the inserted key");

        // Read the keys written to the backup
        let mut written_keys = backup.get_keys()?;
        let first_key = written_keys.next().unwrap();

        // Ensure key that was written was retrieved
        assert_eq!(first_key, RedisKey::from("test-key"), "first written key is accurate");

        store.cleanup()?;

        Ok(())
    }

    /// values that were backed up should be retrievable (BROKEN)
    #[test]
    fn test_read_written_values() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let key = RedisKey::from("test-key");
        let p = (key.clone(), vec![0u8,1u8,2u8,3u8, 4u8, 5u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;
        assert_eq!(backup.get_key_count()?, 1, "backup contains the inserted key");

        // Read the keys & values written to the backup
        let keys = vec!(key).into_iter();
        let mut pairs = backup.get_values(Box::new(keys))?;
        let (key, maybe_val) = pairs.next().unwrap();

        // Ensure the key for the pari that was written is correct
        assert_eq!(key, p.0, "key matches");
        // Ensure that the value was returned and is correct
        assert_eq!(maybe_val, Ok(p.1), "value matches");

        assert!(true, "it works");

        store.cleanup()?;

        Ok(())
    }

    /// get_or_create() on an existing backup with bad version data should fail
    #[test]
    fn test_goc_corrupted_version() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;

        // We have to use knowledge of the internals here to derive the name of the backup folder
        // we get the path to the version file *inside* a specific backup
        let version_file_path = store.get_root_path()?
            .join(BACKUPS_FOLDER_NAME)
            .join(backup.get_name())
            .join(VERSION_FILE_NAME);
        fs::write(version_file_path, "NOPE").map_err(|_| String::from("failed to write corrupt version file"))?;

        // Attempting to get_or_create (the existing backup)
        match store.get_or_create_backup(backup_name) {
            Err(BackupStoreError::FailedToCreateBackup(reason)) => {
                assert!(reason.contains("version file contents invalid"));
                store.cleanup()?;
                Ok(())
            },
            Err(e) => Err(format!("wrong error: {}", e)),
            Ok(_) => Err(String::from("init should have failed with an invalid version file"))
        }
    }

    /// get_or_create() with an corrupted backup (no kv folder)
    #[test]
    fn test_goc_corrupted_kv_folder() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;

        // We have to use knowledge of the internals here to derive the name of the backup folder
        // we get the path to the version file *inside* a specific backup
        let kv_folder_path = store.get_root_path()?
            .join(BACKUPS_FOLDER_NAME)
            .join(backup.get_name())
            .join(FOLDER_BACKUP_KEYS_SUBFOLDER_NAME);
        fs::remove_dir_all(kv_folder_path).map_err(|_| String::from("failed to remove kv folder"))?;

        // Attempting to get_or_create (the existing backup)
        match store.get_or_create_backup(backup_name) {
            Err(BackupStoreError::FailedToCreateBackup(reason)) => {
                assert!(reason.contains("key dump directory is missing"));
                store.cleanup()?;
                Ok(())
            },
            Err(e) => Err(format!("wrong error: {}", e)),
            Ok(_) => Err(String::from("init should have failed with an invalid version file"))
        }
    }

    /// write_pairs() should not create duplicate keys when written twice
    #[test]
    fn test_write_pairs_duplicate_keys_in_keyfile() -> Result<(), String> {
        let mut store = make_test_store()?;
        store.init()?;

        // Create backup
        let backup_name = BackupName::from("test-backup");
        let backup = store.get_or_create_backup(backup_name.clone())?;

        // Add a key to the backup
        let p = (RedisKey::from("test-key"), vec![0u8,0u8,0u8,0u8,0u8,0u8]);
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;

        // Add the same key again
        let pairs = Box::from(vec!(p.clone()).into_iter());
        let _ = backup.write_pairs(pairs)?;

        // We have to use knowledge of the internals here to derive the name of the backup folder
        // we get the path to the version file *inside* a specific backup
        let keys_file_path = store.get_root_path()?
            .join(BACKUPS_FOLDER_NAME)
            .join(backup.get_name())
            .join(KEY_LISTING_FILE_NAME);

        // Open the key listing file that should be stored alongside the backup
        let key_listing = File::open(keys_file_path)
            .map_err(|e| BackupError::UnexpectedError(format!("failed to open keys listing file: {}", e)))?;

        let line_count = BufReader::new(key_listing)
            .lines()
            .into_iter()
            .count();

        assert_eq!(line_count, 1, "there is only one key");

        Ok(())
    }

}
