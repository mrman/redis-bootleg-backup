pub mod folder;
pub mod sqlite;
pub mod s3;

use std::collections::HashMap;

use crate::types::{BackupStoreType, BackupStore};

pub fn build_backup_store(
    store_type: BackupStoreType,
    env_map: &HashMap<String, String>,
) -> Result<Box<dyn BackupStore>, String> {
    Ok(
        match store_type {
            BackupStoreType::FolderV1 => Box::from(folder::v1::FolderStore::new(env_map)?),
            BackupStoreType::SQLiteV1 => Box::from(sqlite::v1::SQLiteStore::new(env_map)?),
            BackupStoreType::S3V1 => Box::from(s3::v1::S3Store::new(env_map)?),
        }
    )
}
