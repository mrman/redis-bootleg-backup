use structopt::StructOpt;

use redis_bootleg_backup::types::{RedisBootlegBackupOpts, SubCommand};
use redis_bootleg_backup::{backup, restore};

fn main() -> Result<(), String> {
    env_logger::init();

    // build a map of the environment variables (serving as a flexible grab-bag of configuration)
    let env_map = std::env::vars().collect();

    // Build args for the actual command line
    let opt = RedisBootlegBackupOpts::from_args();

    match opt.cmd {
        SubCommand::Backup{uri, name} => {
            let _ = backup(opt.executor_type, opt.store_type, uri, name, &env_map)?;
        }

        SubCommand::Restore{uri, name} => {
            let _ = restore(opt.executor_type, opt.store_type, uri, name, &env_map)?;
        }
    }

    Ok(())
}
