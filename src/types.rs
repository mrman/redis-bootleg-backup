use structopt::StructOpt;
use std::str::FromStr;
use std::error::Error;
use std::fmt::{Formatter, Display, Result as FormatResult};
use std::string::ToString;
use semver::Version;
use thiserror::Error;

// General aliases
pub type BackupName = String;
pub type RedisBinaryValue = Vec<u8>;
pub type RedisKey = String;
pub type RedisURI = String;
pub type Reason = String;

//////////////////////
// Backup executors //
//////////////////////

/// Type of executor
#[derive(Debug, PartialEq)]
pub enum BackupExecutorType {
    DirectV1
}

impl FromStr for BackupExecutorType {
    type Err = BackupExecutorParseError;

    fn from_str(s: &str) -> Result<BackupExecutorType, BackupExecutorParseError> {
        match s.to_lowercase().as_str() {
            "direct-v1" => Ok(BackupExecutorType::DirectV1),
            _ => Err(BackupExecutorParseError{})
        }
    }
}

impl ToString for BackupExecutorType {
    fn to_string(&self) -> String {
        match self {
            BackupExecutorType::DirectV1 => String::from("direct-v1"),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct BackupExecutorParseError;
impl Error for BackupExecutorParseError {}
impl Display for BackupExecutorParseError {
    fn fmt(&self, f: &mut Formatter) -> FormatResult {
        write!(f, "failed to parse backup executor type")
    }
}

/// General errors emitted by a BackupExecutor
#[derive(Debug, PartialEq, Error)]
pub enum BackupExecutorError {
    #[error("creating client field: {0}")]
    ClientCreationFailed(Reason),
    #[error("conenction failed: {0}")]
    ConnectionFailed(Reason),
    #[error("client not connected")]
    ClientNotConnected,
    #[error("invalid configuration: {0}")]
    InvalidConfiguration(Reason),
    #[error("backup executor was not initialized")]
    UninitializedExecutor,
    #[error("unexpected error: {0}")]
    UnexpectedError(Reason),
    #[error("not implemented")]
    NotImplemented,
}

impl From<BackupExecutorError> for String {
    fn from(err: BackupExecutorError) -> String {
        format!("{}", err)
    }
}

/// Executors are components that can take backups and restores
pub trait BackupExecutor {
    /// Get the backup executor type
    fn get_type(&self) -> BackupExecutorType;

    /// Get the version of the executor itself
    fn get_executor_version(&self) -> Result<Version, BackupExecutorError>;

    /// Initialize the executor, performing any necessary setup/locking/etc
    fn init(&mut self, uri: RedisURI) -> Result<(), BackupExecutorError>;

    /// Cleanup the backend
    fn cleanup(&mut self) -> Result<(), BackupExecutorError>;
}

/// Errors that occur during backups
#[derive(Debug, PartialEq, Error)]
pub enum BackupError {
    #[error("failed to back up key ['{0}']: {1}")]
    KeyBackupFailed(RedisKey, Reason),
    #[error("failed to retrieve value for key ['{0}']: {1}")]
    ValueRetrievalFailed(RedisKey, Reason),
    #[error("failed to connect: {0}")]
    ConnectionFailure(Reason),
    #[error("unexpected error: {0}")]
    UnexpectedError(Reason),
    #[error("unsupported version: {0}")]
    UnsupportedVersion(Reason),
    #[error("corrupted backup: {0}")]
    CorruptedBackup(Reason),
    #[error("executor error: {0}")]
    ExecutorError(BackupExecutorError),
    #[error("store error: {0}")]
    StoreError(BackupStoreError),
    #[error("not implemented")]
    NotImplemented,
}

impl From<BackupStoreError> for BackupError {
    fn from(err: BackupStoreError) -> BackupError {
        BackupError::StoreError(err)
    }
}

impl From<BackupExecutorError> for BackupError {
    fn from(err: BackupExecutorError) -> BackupError {
        BackupError::ExecutorError(err)
    }
}

impl From<BackupError> for String {
    fn from(err: BackupError) -> String {
        format!("{}", err)
    }
}

/// Executors that can perform backups
pub trait CanBackup: BackupExecutor {
    /// Retrieve the available keys that could be backed up
    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError>;

    /// Perform a partial backup of a given subset of keys
    fn partial_backup(&self, keys: Box<dyn Iterator<Item=RedisKey>>, backup: Box<dyn Backup>) -> Result<Box<dyn Backup>, BackupError>;

    /// A full backup
    fn full_backup(&self, backup: Box<dyn Backup>) -> Result<Box<dyn Backup>, BackupError> {
        let backup = self.partial_backup(self.get_keys()?, backup)?;
        Ok(backup)
    }
}

/// Errors that occur during restores
#[derive(Debug, PartialEq, Error)]
pub enum RestoreError {
    #[error("failed to restore key ['{0}']: {1}")]
    KeyRestoreFailed(RedisKey, Reason),
    #[error("failed to connect: {0}")]
    ConnectionFailure(Reason),
    #[error("unexpected error: {0}")]
    UnexpectedError(Reason),
    #[error("invalid configuration: {0}")]
    InvalidConfiguration(Reason),
    #[error("executor error: {0}")]
    ExecutorError(BackupExecutorError),
    #[error("store error: {0}")]
    StoreError(BackupStoreError),
    #[error("not implemented")]
    NotImplemented,
}

impl From<BackupStoreError> for RestoreError {
    fn from(err: BackupStoreError) -> RestoreError {
        RestoreError::StoreError(err)
    }
}

impl From<RestoreError> for String {
    fn from(err: RestoreError) -> String {
        format!("{}", err)
    }
}

/// Executors that can perform restores
pub trait CanRestore: BackupExecutor {
    /// Retrieve the available keys that could be restored from a given backup
    fn get_keys_from_backup(&self, backup: Box<dyn Backup>) -> Result<Box<dyn Iterator<Item=RedisKey>>, RestoreError>;

    /// Perform a partial restore of a given subset of keys
    fn partial_restore(&self, keys: Box<dyn Iterator<Item=RedisKey>>, backup: Box<dyn Backup>) -> Result<Box<dyn Backup>, RestoreError>;

    /// A full restore
    fn full_restore(&self, backup: Box<dyn Backup>) -> Result<Box<dyn Backup>, RestoreError> {
        let keys = backup.get_keys()
            .map_err(|e| RestoreError::UnexpectedError(format!("Failed to get keys from backup: {:#?}", e)))?;
        let backup = self.partial_restore(keys, backup)?;
        Ok(backup)
    }
}

/// A possibly in-progress backup
pub trait Backup {
    /// Get the version of this backup (which matches the store version at creation)
    fn get_version(&self) -> Result<Version, BackupError>;

    /// Get the name of the backup
    fn get_name(&self) -> BackupName;

    /// Get the store type the backup was made with/for
    fn get_store_type(&self) -> BackupStoreType;

    /// Get the number of keys stored
    fn get_key_count(&self) -> Result<u32, BackupError>;

    // Write a single kv pair to the backup
    fn write_pair(&self, key: RedisKey, value: RedisBinaryValue) -> Result<(), BackupError>;

    /// Write an iterator of kv pairs to the backup
    fn write_pairs(&self, pairs: Box<dyn Iterator<Item=(RedisKey, RedisBinaryValue)>>) -> Result<(), BackupError>;

    /// Get all keys that are contained in the backup
    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError>;

    /// Get a single value for a given key
    fn get_value(&self, key: RedisKey) -> Result<RedisBinaryValue, BackupError>;

    /// Get one or more values for a given set of keys
    fn get_values(&self, keys: Box<dyn Iterator<Item=RedisKey>>) -> Result<Box<dyn Iterator<Item=(RedisKey, Result<RedisBinaryValue, BackupError>)>>, BackupError>;
}

///////////////////
// Backup Stores //
///////////////////

/// Type of store
#[derive(Debug, PartialEq)]
pub enum BackupStoreType {
    FolderV1,
    SQLiteV1,
    S3V1,
}

#[derive(Debug, PartialEq)]
pub struct BackupStoreParseError;
impl Error for BackupStoreParseError {}
impl Display for BackupStoreParseError {
    fn fmt(&self, f: &mut Formatter) -> FormatResult {
        write!(f, "failed to parse backup store type")
    }
}

impl FromStr for BackupStoreType {
    type Err = BackupStoreParseError;

    fn from_str(s: &str) -> Result<BackupStoreType, BackupStoreParseError> {
        match s.to_lowercase().as_str() {
            "folder-v1" => Ok(BackupStoreType::FolderV1),
            "sqlite-v1" => Ok(BackupStoreType::SQLiteV1),
            "s3-v1" => Ok(BackupStoreType::S3V1),
            _ => Err(BackupStoreParseError{})
        }
    }
}

impl ToString for BackupStoreType {
    fn to_string(&self) -> String {
        match self {
            BackupStoreType::FolderV1 => String::from("folder-v1"),
            BackupStoreType::SQLiteV1 => String::from("sqlite-v1"),
            BackupStoreType::S3V1 => String::from("s3-v1"),
        }
    }
}

/// General errors emitted by a store
#[derive(Debug, PartialEq, Error)]
pub enum BackupStoreError {
    #[error("failed to create backup: {0}")]
    FailedToCreateBackup(Reason),
    #[error("no such backup: {0}")]
    NoSuchBackup(Reason),
    #[error("invalid configuration: {0}")]
    InvalidConfiguration(Reason),
    #[error("uninitialized store: {0}")]
    UninitializedStore(Reason),
    #[error("unexpected error: {0}")]
    UnexpectedError(Reason),
    #[error("not implemented")]
    NotImplemented,
}

impl From<BackupStoreError> for String {
    fn from(err: BackupStoreError) -> String {
        format!("{}", err)
    }
}

/// Storage backends that ingest redis backups
pub trait BackupStore {
    /// Get the type of the backup store
    fn get_type(&self) -> BackupStoreType;

    /// Get the version of the store itself
    fn get_store_version(&self) -> Result<Version, BackupStoreError>;

    /// Initialize the storage store, performing any necessary setup/locking/etc
    fn init(&mut self) -> Result<(), BackupStoreError>;

    /// Retrieve a backup (creating it if it doesn't exist)
    fn get_or_create_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError>;

    /// Get a backup
    fn get_backup(&self, backup_name: BackupName) -> Result<Box<dyn Backup>, BackupStoreError>;

    /// Cleanup the connection to the storage store
    fn cleanup(&mut self) -> Result<(), BackupStoreError>;
}

//////////////////
// Command line //
//////////////////

/// Options for structopt parsing
#[derive(Debug, StructOpt)]
#[structopt(name="redis-bootleg-backup", about = "Brute force key-enumeration backups for redis.")]
pub struct RedisBootlegBackupOpts {
    /// Activate debug mode
    #[structopt(short, long, env)]
    pub debug: bool,

    /// Type of executor to use
    #[structopt(short, long, env, default_value = "direct-v1", parse(try_from_str))]
    pub executor_type: BackupExecutorType,

    /// Type of backup store
    #[structopt(short, long, env, default_value = "folder-v1", parse(try_from_str))]
    pub store_type: BackupStoreType,

    #[structopt(subcommand)]
    pub cmd: SubCommand,
}

/// Subcommands for structopt
#[derive(Debug, StructOpt, Clone)]
#[structopt(about = "the redis backup tool you shouldn't be using")]
pub enum SubCommand {
    /// Perform a backup
    Backup {
        /// URI of redis server to pull the backend from
        #[structopt(short, long, default_value = "redis://localhost:6379", env)]
        uri: RedisURI,

        /// Name of the backup
        #[structopt(short, long, env)]
        name: BackupName,
    },

    /// Perform a restore
    Restore {
        /// URI of redis server to restore an existing backup to
        #[structopt(short, long, default_value = "redis://localhost:6379", env)]
        uri: RedisURI,

        /// Name of the backup
        #[structopt(short, long, env)]
        name: BackupName,
    },
}

mod tests {
    #[cfg(test)]
    use super::{BackupStoreParseError, BackupStoreType};
    #[cfg(test)]
    use std::str::FromStr;

    /// BackupStoreType::FolderV1 parse tests
    #[test]
    fn test_backend_type_folder_str_parse() {
        let spellings: Vec<String> = vec!("folder-v1".to_string(), "Folder-v1".to_string(), "FOLDER-v1".to_string());
        for s in spellings {
            assert!(
                BackupStoreType::from_str(&s).unwrap() == BackupStoreType::FolderV1,
                "'folder-v1' parses into BackupStoreType",
            );
        }
    }

    /// BackupStoreType bad parse tests
    #[test]
    fn test_backend_type_unknown_str_parse() {
        assert!(
            BackupStoreType::from_str("unknown").unwrap_err() == BackupStoreParseError{},
            "unknown",
        );
    }

}
