pub mod direct;

use std::collections::HashMap;

use crate::types::{BackupExecutorType, CanBackup, CanRestore};

pub fn build_backup_executor(
    executor_type: BackupExecutorType,
    _env_map: &HashMap<String, String>,
) -> Result<Box<dyn CanBackup>, String> {
    // Executors should pull their options from CLI
    match executor_type {
        BackupExecutorType::DirectV1 => Ok(Box::from(direct::v1::DirectExecutor::new())),
    }
}

pub fn build_restore_executor(
    executor_type: BackupExecutorType,
    _env_map: &HashMap<String, String>,
) -> Result<Box<dyn CanRestore>, String> {
    // Executors should pull their options from CLI
    match executor_type {
        BackupExecutorType::DirectV1 => Ok(Box::from(direct::v1::DirectExecutor::new())),
    }
}
