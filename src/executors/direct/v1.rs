use log::{debug, info};
use semver::Version;
use redis::Commands;

use crate::types::*;

/// Direct redis connection executor
pub struct DirectExecutor {
    uri: Option<RedisURI>,
    client: Option<redis::Client>,
}

impl DirectExecutor {
    /// Create a direct executor
    pub fn new() -> DirectExecutor {
        DirectExecutor{
            uri: None,
            client: None,
        }
    }
}

const VERSION: &'static str = "1.0.0";

/// Executors are components that can take backups and restores
impl BackupExecutor for DirectExecutor {
    /// Get the backup backend enum type
    fn get_type(&self) -> BackupExecutorType {
        BackupExecutorType::DirectV1
    }

    /// Get the version of the executor itself
    fn get_executor_version(&self) -> Result<Version, BackupExecutorError> {
        Version::parse(VERSION)
            .map_err(|_| BackupExecutorError::UnexpectedError(String::from("Failed to retrieve version")))
    }

    /// Initialize the executor, performing any necessary setup/locking/etc
    fn init(&mut self, uri: RedisURI) -> Result<(), BackupExecutorError> {
        // Create a client
        let client = redis::Client::open(uri.as_str())
            .map_err(|e| BackupExecutorError::ClientCreationFailed(format!("failed to create redis client: {}", e.to_string())))?;
        self.client = Some(client);
        self.uri = Some(uri.clone());

        Ok(())
    }

    /// Cleanup the backend
    fn cleanup(&mut self) -> Result<(), BackupExecutorError> {
        Ok(())
    }
}

impl DirectExecutor {
    /// Get a connection to the redis instance
    fn get_connection(&self) -> Result<redis::Connection, BackupExecutorError> {
        if self.client.as_ref().is_none() {
            return Err(BackupExecutorError::ClientNotConnected);
        }

        let client = self.client.as_ref().unwrap().clone();
        let conn = client.get_connection()
            .map_err(|e| BackupExecutorError::ConnectionFailed(format!("failed to connect to redis: {}", e.to_string())))?;

        Ok(conn)
    }
}

impl CanBackup for DirectExecutor {
    fn get_keys(&self) -> Result<Box<dyn Iterator<Item=RedisKey>>, BackupError> {
        debug!("[executors/direct-v1] creating connection to redis...");
        let mut conn = self.get_connection()
            .map_err(|e| BackupError::ExecutorError(e))?;

        let msg = "[executors/direct-v1] retrieving keys...";
        debug!("{}", msg);
        println!("{}", msg);

        let keys = conn.keys::<RedisKey, Vec<RedisKey>>(String::from("*"))
            .map_err(|e| BackupError::UnexpectedError(format!("KEYS * command failed: {}",e.to_string())))?
            .into_iter();

        debug!("[executors/direct-v1] successfully retrieved keys connection...");
        Ok(Box::from(keys))
    }

    fn partial_backup(
        &self,
        mut keys: Box<dyn Iterator<Item=RedisKey>>,
        backup: Box<dyn Backup>
    ) -> Result<Box<dyn Backup>, BackupError> {
        debug!("[executors/direct-v1] creating connection to redis...");
        let mut conn = self.get_connection()
            .map_err(|e| BackupError::ExecutorError(e))?;

        info!("[executors/direct-v1] backing up keys from redis instance...");
        let mut count = 0;
        while let Some(k) = keys.as_mut().next() {
            let msg = format!("[executors/direct-v1] writing key [{}]...", &k);
            println!("{}", msg);
            debug!("{}", msg);
            let value = conn.get(k.as_str())
                .map_err(|e| BackupError::KeyBackupFailed(
                    String::from(k.as_str()),
                    e.to_string(),
                ))?;

            let _ = backup.write_pair(k, value)?;

            count += 1;
        }

        info!("[executors/direct-v1] successfully backed up {} keys from redis instance", count);
        Ok(backup)
    }
}

impl CanRestore for DirectExecutor {
    fn get_keys_from_backup(&self, backup: Box<dyn Backup>) -> Result<Box<dyn Iterator<Item=RedisKey>>, RestoreError> {
        backup.get_keys()
            .map_err(|e| RestoreError::UnexpectedError(format!("failed to get keys from backup: {}", e)))
    }

    fn partial_restore(
        &self,
        mut keys: Box<dyn Iterator<Item=RedisKey>>,
        backup: Box<dyn Backup>,
    ) -> Result<Box<dyn Backup>, RestoreError> {
        debug!("[executors/direct-v1] creating connection to redis...");
        let mut conn = self.get_connection()
            .map_err(|e| RestoreError::ExecutorError(e))?;

        // Go through the list of keys
        info!("[executors/direct-v1] restoring keys to redis instance...");
        let mut count = 0;
        while let Some(k) = keys.as_mut().next() {
            debug!("[executors/direct-v1] creating connection to redis...");
            // Retrieve the key's value from the backup
            let value = backup.get_value(k.clone())
                .map_err(|e| RestoreError::KeyRestoreFailed(String::from(k.as_str()), format!("{}", e)))?;

            // Write the backup
            conn.set(k.as_str(), value)
                .map_err(|e| RestoreError::KeyRestoreFailed(String::from(k.as_str()), format!("{}", e)))?;

            count += 1;
        }

        info!("[executors/direct-v1] successfully restored {} keys to redis instance", count);
        Ok(backup)
    }
}
