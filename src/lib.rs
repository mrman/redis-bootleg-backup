pub mod types;
pub mod executors;
pub mod stores;

use std::collections::HashMap;
use types::{Backup, RedisURI, BackupName, BackupExecutorType, BackupStoreType};
use executors::{build_backup_executor, build_restore_executor};
use stores::build_backup_store;

/// Perform a redis backup, performing a cleanup
pub fn backup(
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
    uri: RedisURI,
    name: BackupName,
    env_map: &HashMap<String, String>,
) -> Result<Box<dyn Backup>, String> {
    // Build store
    println!("[main] building store [{:?}]...", store_type);
    let mut store = build_backup_store(store_type, env_map)?;
    store.init()?;

    // Build executor
    println!("[main] building executor [{:?}]...", executor_type);
    let mut executor = build_backup_executor(executor_type, env_map)?;
    executor.init(uri)?;

    // Create a backup
    println!("[main] get/create backup [{}]...", &name);
    let backup = Box::from(store.get_or_create_backup(name)?);

    // Perform a full backup with the executor
    println!("[main] starting full backup...");
    let backup = executor.full_backup(backup)?;

    println!("[main] successfully completed full backup");

    // Cleanup
    store.cleanup()?;
    executor.cleanup()?;

    Ok(backup)
}

pub fn restore(
    executor_type: BackupExecutorType,
    store_type: BackupStoreType,
    uri: RedisURI,
    name: BackupName,
    env_map: &HashMap<String, String>,
) -> Result<Box<dyn Backup>, String> {
    // Build store
    println!("[main] building store [{:?}]...", store_type);
    let mut store = build_backup_store(store_type, &env_map)?;
    store.init()?;

    // Build executor
    println!("[main] building executor [{:?}]...", executor_type);
    let mut executor = build_restore_executor(executor_type, &env_map)?;
    executor.init(uri)?;

    // Get the backup
    println!("[main] get/create backup [{}]...", &name);
    let backup = store.get_backup(name)?;

    // Perform a full restore with the executor
    println!("[main] starting full restore...");
    let backup = executor.full_restore(Box::from(backup))?;

    println!("[main] successfully completed full restore");

    // Cleanup
    store.cleanup()?;
    executor.cleanup()?;

    Ok(backup)
}
